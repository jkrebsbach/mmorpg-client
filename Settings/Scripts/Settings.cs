using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Assets.Location.Scripts;
using Assets.Settings.Scripts;

public class Settings : MonoBehaviour {
	private bool _showItems;
    private bool _showQuests;
	private bool _showAbilities;
	private bool _showEquipment;
	private bool _showEquipmentSelection;
	private bool _showStatus;
	private bool _showQuit;
    private bool _showCredits;
	private bool _chooseTarget;

	private bool _chooseLeftHand;
	private bool _chooseRightHand;
	private bool _chooseHead;
	private bool _chooseBody;
    private bool _chooseRanged;

	public Texture2D backButton;
	public GUISkin guiSkin;

    private bool _mute;
    public Texture2D muteButton;
    public Texture2D unmuteButton;

    private int toolBarInt = -1;
	private Vector2 scrollPosition = Vector2.zero;
	private ClientSettings _clientSettings = null;

    private int _chosenAbility = 0;
    private bool _spendSkillPoints = false;
    private bool _learningSkill = false;
    private List<Ability> _characterTypeAbilities = null;

    private bool _loadingQuests = false;
    private List<Quest> _characterQuests = null;

    private Rect _selectionScrollRect;
    private Rect _detailTextRect;

    private float _lastScreenWidth = 0f;
    private int _detailColumns = 2;
    private float _buttonWidth = 250f;
    bool _skinInitialized = false;

	// Use this for initialization
	protected void Start () {
		_clientSettings = FindObjectOfType<ClientSettings> ();

        _characterTypeAbilities = Ability.GetCharacterTypeAbilityLibrary(_clientSettings.CharacterTypeID);

        if (PlayerPrefs.HasKey("Mute"))
            _mute = (PlayerPrefs.GetInt("Mute") == 1);
        else
        {
            _clientSettings.Mute = false;
            _mute = false;
        }

        
	}

    protected void Update()
    {
        while (_clientSettings.QuestQueue.Count > 0)
        {
            string[] questMsg = _clientSettings.QuestQueue.Pop();

            if (_characterQuests == null)
                _characterQuests = new List<Quest>();

            for (int index = 1; index < questMsg.Length; index++)
            {
                string[] questItem = questMsg[index].Split('~');
                if (questItem.Length > 1)
                {
                    int questID = int.Parse(questItem[0]);
                    int status = int.Parse(questItem[1]);

                    var quest = Quest.GetQuest(questID);
                    quest.QuestStatus = (Quest.QuestStatusEnum)status;

                    _characterQuests.Add(quest);
                }
            }
        }
        while (_clientSettings.SkillLearnedQueue.Count > 0)
        {
            string[] skillMsg = _clientSettings.SkillLearnedQueue.Pop();

            int abilityID = int.Parse(skillMsg[1]);
            _learningSkill = false;
            _spendSkillPoints = false;
        }
    }

    GUIStyle labelStyle = null;
    GUIStyle buttonStyle = null;

	// Update is called once per frame
	protected void OnGUI () {
        AdjustScale();

        if (GUI.Button(new Rect(10, 10, 50, 50), _mute ? unmuteButton : muteButton))
        {
            _mute = !_mute;
            AudioListener.pause = _mute;
            _clientSettings.Mute = _mute;
        } 
        
        if (GUI.Button(new Rect(Screen.width * .30f, Screen.height * .9f, Screen.width * .4f, Screen.height * .05f), "Back to Game", buttonStyle))
        {
            if (_clientSettings.CurrentLocation > 0)
            {
                Application.LoadLevel("LocationScene");
            }
            else
            {
                Application.LoadLevel("OverworldScene");
            }
		}

		if (_chooseTarget) {
			ShowDetails();
		} 

		if (_showItems) {
			ShowItems ();
        } else if (_showQuests) {
            ShowQuests();
        } else if (_showAbilities) {
			ShowAbilities ();
		} else if (_showEquipment) {
			ShowEquipment ();
		} else if (_showEquipmentSelection) {
			ShowEquipmentSelection();
		} else if (_showStatus) {
			ShowStatus ();
		} else  if (_showCredits) {
            ShowCredits();
        } else if (_showQuit) {
			ShowQuit ();
        }

        string[] selStrings = new string[] { "Items", "Quests", "Abilities", "Equipment", "Status", "Credits", "Quit" };
		int newToolBar = GUI.SelectionGrid (new Rect((float)(Screen.width *.75), 5f, (float)(Screen.width *.25 - 10), Screen.height), toolBarInt, selStrings, 1, GUI.skin.button);

        if (GUI.changed)
        {
            if (_chooseLeftHand || _chooseRightHand || _chooseBody || _chooseHead || _chooseRanged)
            {
                _showEquipmentSelection = true;
                _showEquipment = false;
            }

            if (newToolBar != toolBarInt)
            {
                toolBarInt = newToolBar;
                ResetFlags();
                switch (toolBarInt)
                {
                    case 0: // Load items
                        _showItems = true;
                        break;
                    case 1: // Load quests
                        _showQuests = true;
                        break;
                    case 2: // Load abilities
                        _showAbilities = true;
                        break;
                    case 3: // Load equipment
                        if (_chooseLeftHand || _chooseRightHand || _chooseBody || _chooseHead)
                            _showEquipmentSelection = true;
                        else
                            _showEquipment = true;
                        break;
                    case 4: // Load status
                        _showStatus = true;
                        break;
                    case 5: // Credits
                        _showCredits = true;
                        break;
                    case 6: // Exit application
                        _showQuit = true;
                        break;
                }
            }
        }
	}

    private void AdjustScale()
    {
        if (_lastScreenWidth != Screen.width)
        {
            _lastScreenWidth = Screen.width;

            _selectionScrollRect = new Rect(Screen.width * .1f, 10f, Screen.width * .65f, Screen.height * .5f);
            _detailTextRect = new Rect(Screen.width * .1f, Screen.height * .55f, Screen.width * .65f, Screen.height * .2f);
            
            if (!_skinInitialized)
            {
                GUI.skin = guiSkin;
                labelStyle = GUI.skin.label;
                buttonStyle = GUI.skin.button;

                _skinInitialized = true;
            }


            // Custom formatting for small screen sizes
            if (Screen.width < 768)
            {
                _detailColumns = 1;
                _buttonWidth = 350;

                labelStyle.fontSize = 15.DpToPixel();
                buttonStyle.fontSize = 15.DpToPixel();
                GUI.skin.verticalScrollbar.fixedWidth = Screen.width * 0.05f;
                GUI.skin.verticalScrollbarThumb.fixedWidth = Screen.width * 0.05f;
            }
            else
            {
                // Default formatting
                _detailColumns = 2;
                _buttonWidth = 250;

                labelStyle.fontSize = 0;
                buttonStyle.fontSize = 0;

                GUI.skin.verticalScrollbar.fixedWidth = 15f;
                GUI.skin.verticalScrollbarThumb.fixedWidth = 15f;
            }
        }
    }

	private void ShowItems() {
        int rowCount = (_clientSettings.Inventory.Count / _detailColumns) + 1;
        scrollPosition = GUI.BeginScrollView(_selectionScrollRect, scrollPosition, 
		                                     new Rect(0, 0, Screen.width * .6f, rowCount * Screen.height * .15f));
		
		int abilityRow = 0;
		int abilityCol = 0;
		foreach (InventoryItem item in _clientSettings.Inventory) {
			
			if (GUI.Button (new Rect (abilityCol * Screen.width * .3f, abilityRow * Screen.height * .15f,
                                      _buttonWidth, Screen.height * .1f), item.Name + " - " + item.Quantity, buttonStyle))
            {
				_chosenAbility = item.AbilityID;
				_chooseTarget = true;
			}
			
			abilityCol++;
			if (abilityCol % _detailColumns == 0) {
				abilityRow++;
				abilityCol = 0;
			}
		}
		
		GUI.EndScrollView ();
	}

    private void ShowQuests()
    {
        Rect questRect = new Rect(Screen.width * .1f, Screen.height * .1f, Screen.width * .8f, Screen.height * .3f);
        
        if (_characterQuests == null)
        {
            if (!_loadingQuests)
            {
                _clientSettings.QuestQueue.Clear();
                _loadingQuests = true;
                GameServer.RetrieveQuests(-1);
            }

            GUI.Label(questRect, "Loading quests: ...", labelStyle);
            return;
        }
        if (_characterQuests.Count == 0)
        {
            GUI.Label(questRect, "No quests yet.", labelStyle);
            return;
        }

        int rowCount = (_characterQuests.Count / _detailColumns) + 1;
        scrollPosition = GUI.BeginScrollView(_selectionScrollRect, scrollPosition,
                                             new Rect(0, 0, Screen.width * .6f, rowCount * Screen.height * .2f));

        int questRow = 0;
        int questCol = 0;
        foreach (Quest quest in _characterQuests)
        {

            if (GUI.Button(new Rect(questCol * Screen.width * .3f, questRow * Screen.height * .15f,
                                      _buttonWidth, Screen.height * .1f), quest.QuestName, buttonStyle))
            {
                _chosenAbility = quest.QuestID;
                _chooseTarget = true;
            }

            questCol++;
            if (questCol % _detailColumns == 0)
            {
                questRow++;
                questCol = 0;
            }
        }

        GUI.EndScrollView();
    }

	private void ShowAbilities() {

        if (_spendSkillPoints)
        {
            int rowCount = (_characterTypeAbilities.Count / _detailColumns) + 1;
            scrollPosition = GUI.BeginScrollView(_selectionScrollRect, scrollPosition,
                                                new Rect(0, 0, Screen.width * .6f, rowCount * Screen.height * .15f));

            int abilityRow = 0;
            int abilityCol = 0;
            foreach (Ability ability in _characterTypeAbilities)
            {
                if (GUI.Button(new Rect(abilityCol * Screen.width * .3f, abilityRow * Screen.height * .15f,
                                          _buttonWidth, Screen.height * .1f), ability.FullName(ability.Level + 1), buttonStyle))
                {
                    _chosenAbility = ability.AbilityID;
                    _chooseTarget = true;
                }

                abilityCol++;
                if (abilityCol % _detailColumns == 0)
                {
                    abilityRow++;
                    abilityCol = 0;
                }
            }

            GUI.EndScrollView();

            if (GUI.Button(new Rect(Screen.width * .55f, Screen.height * .75f, Screen.width * .2f, Screen.height * .1f), "Back"))
            {
                _spendSkillPoints = false;
            }
        }
        else
        {
            int rowCount = (_clientSettings.Spells.Count / _detailColumns) + 1;
            scrollPosition = GUI.BeginScrollView(_selectionScrollRect, scrollPosition,
                                                new Rect(0, 0, Screen.width * .6f, rowCount * Screen.height * .2f));

            int abilityRow = 0;
            int abilityCol = 0;
            foreach (Ability ability in _clientSettings.Spells)
            {

                if (GUI.Button(new Rect(abilityCol * Screen.width * .3f, abilityRow * Screen.height * .15f,
                                          Screen.width * .2f, Screen.height * .1f), ability.FullName(ability.Level), buttonStyle))
                {
                    _chosenAbility = ability.AbilityID;
                    _chooseTarget = true;
                }

                abilityCol++;
                if (abilityCol % _detailColumns == 0)
                {
                    abilityRow++;
                    abilityCol = 0;
                }
            }

            GUI.EndScrollView();

            // Button to spend skill points
            if (GUI.Button(new Rect(Screen.width * .05f, Screen.height * .80f, Screen.width * .5f, Screen.height * .1f),
                string.Format("Spend Skill Points\r\n({0} available)", _clientSettings.SkillPoints), buttonStyle))
            {
                _spendSkillPoints = true;
            }
        }
	}

	private void ShowEquipment() {
		InventoryItem itemLeftHand = null;
		InventoryItem itemRightHand = null;
		InventoryItem itemHead = null;
		InventoryItem itemBody = null;
        InventoryItem itemRanged = null;

		foreach (InventoryItem item in _clientSettings.Inventory) {
			if (item.AbilityID == _clientSettings.EquipLeftHand) itemLeftHand = item;
			if (item.AbilityID == _clientSettings.EquipRightHand) itemRightHand = item;
			if (item.AbilityID == _clientSettings.EquipHead) itemHead = item;
			if (item.AbilityID == _clientSettings.EquipBody) itemBody = item;
            if (item.AbilityID == _clientSettings.EquipRanged) itemRanged = item;
		}

		GUI.Label (new Rect (Screen.width * .1f, Screen.height * .1f, Screen.width * .3f, Screen.height * .08f),
                   "Left Hand: ", labelStyle);
		GUI.Label (new Rect (Screen.width * .1f, Screen.height * .2f, Screen.width * .3f, Screen.height * .08f),
                   "Right Hand: ", labelStyle);
		GUI.Label (new Rect (Screen.width * .1f, Screen.height * .3f, Screen.width * .3f, Screen.height * .08f),
                   "Head: ", labelStyle);
		GUI.Label (new Rect (Screen.width * .1f, Screen.height * .4f, Screen.width * .3f, Screen.height * .08f),
                   "Body: ", labelStyle);
        GUI.Label(new Rect(Screen.width * .1f, Screen.height * .5f, Screen.width * .3f, Screen.height * .08f),
                   "Ranged: ", labelStyle);

        if (GUI.Button(new Rect(Screen.width * .4f, Screen.height * .1f, Screen.width * .3f, Screen.height * .08f), itemLeftHand == null ? "None" : itemLeftHand.Name, buttonStyle))
			_chooseLeftHand = true;
        if (GUI.Button(new Rect(Screen.width * .4f, Screen.height * .2f, Screen.width * .3f, Screen.height * .08f), itemRightHand == null ? "None" : itemRightHand.Name, buttonStyle))
		    _chooseRightHand = true;
        if (GUI.Button(new Rect(Screen.width * .4f, Screen.height * .3f, Screen.width * .3f, Screen.height * .08f), itemHead == null ? "None" : itemHead.Name, buttonStyle))
			_chooseHead = true;
        if (GUI.Button(new Rect(Screen.width * .4f, Screen.height * .4f, Screen.width * .3f, Screen.height * .08f), itemBody == null ? "None" : itemBody.Name, buttonStyle))
		    _chooseBody = true;
        if (GUI.Button(new Rect(Screen.width * .4f, Screen.height * .5f, Screen.width * .3f, Screen.height * .08f), itemRanged == null ? "None" : itemRanged.Name, buttonStyle))
            _chooseRanged = true;
    }

	private void ShowEquipmentSelection() {
		InventoryItem.ItemTypeEnum itemType = InventoryItem.ItemTypeEnum.Hand;

		if (_chooseHead)
			itemType = InventoryItem.ItemTypeEnum.Head;
		else if (_chooseBody)
			itemType = InventoryItem.ItemTypeEnum.Body;
        else if (_chooseRanged)
            itemType = InventoryItem.ItemTypeEnum.Ranged;

		List<InventoryItem> items = new List<InventoryItem> ();
		foreach (InventoryItem item in _clientSettings.Inventory.Where(inv => inv.CharacterTypeID.Contains(_clientSettings.CharacterTypeID))) {
            if (item.ItemType == InventoryItem.ItemTypeEnum.TwoHand && itemType == InventoryItem.ItemTypeEnum.Hand)
            {
                if (item.Quantity > 0)
                {
                    items.Add(item);
                }
            }
			else if (item.ItemType == itemType) {
				if ((_chooseLeftHand && _clientSettings.EquipRightHand == item.AbilityID) ||
					(_chooseRightHand && _clientSettings.EquipLeftHand == item.AbilityID)) {
					// We need two if we are putting the same item in both hands
					if (item.Quantity > 1) {
						items.Add(item);
					}
				} else if (item.Quantity > 0) {
					items.Add (item);
				}
			}
		}

        int rowCount = (_clientSettings.Inventory.Count / _detailColumns) + 1;
        scrollPosition = GUI.BeginScrollView(_selectionScrollRect, scrollPosition, 
		                            new Rect(0, 0, Screen.width * .6f, rowCount * Screen.height * .2f));
		
		int abilityRow = 0;
		int abilityCol = 0;
		foreach (InventoryItem item in items) {
			bool equipped = (_chooseLeftHand && _clientSettings.EquipLeftHand == item.AbilityID) ||
				(_chooseRightHand && _clientSettings.EquipRightHand == item.AbilityID) ||
				(_chooseHead && _clientSettings.EquipHead == item.AbilityID) ||
                (_chooseBody && _clientSettings.EquipBody == item.AbilityID) ||
                (_chooseRanged && _clientSettings.EquipRanged == item.AbilityID);


			if (GUI.Button (new Rect (abilityCol * Screen.width * .3f, abilityRow * Screen.height * .15f, Screen.width * .2f, Screen.height * .1f), 
			                string.Format("{0}{1}", item.Name, equipped ? "\r\n(Equipped)" : string.Empty), buttonStyle)) {

                if (_chooseLeftHand)
                {
                    _clientSettings.EquipLeftHand = item.AbilityID;
                    if (_clientSettings.EquipRightHand > 0 && Ability.GetItem(_clientSettings.EquipRightHand).ItemType == InventoryItem.ItemTypeEnum.TwoHand)
                        _clientSettings.EquipRightHand = 0;
                }
                if (_chooseRightHand)
                {
                    _clientSettings.EquipRightHand = item.AbilityID;
                    if (_clientSettings.EquipLeftHand > 0 && Ability.GetItem(_clientSettings.EquipLeftHand).ItemType == InventoryItem.ItemTypeEnum.TwoHand)
                        _clientSettings.EquipLeftHand = 0;
                }
				if (_chooseHead)
					_clientSettings.EquipHead = item.AbilityID;
				if (_chooseBody)
					_clientSettings.EquipBody = item.AbilityID;
                if (_chooseRanged)
                    _clientSettings.EquipRanged = item.AbilityID;

                if (item.ItemType == InventoryItem.ItemTypeEnum.TwoHand)
                {
                    _clientSettings.EquipLeftHand = item.AbilityID;
                    _clientSettings.EquipRightHand = item.AbilityID;
                }

                GameServer.UpdateEquipment();

                _showEquipmentSelection = false;
                _showEquipment = true;
			}
			
			abilityCol++;
            if (abilityCol % _detailColumns == 0)
            {
				abilityRow++;
				abilityCol = 0;
			}
		}

	    if (items.Count == 0)
	    {
            GUI.Label(new Rect(Screen.width * .1f, Screen.height * .3f, Screen.width * .6f, Screen.height * .1f),
                   "Nothing to equip", labelStyle);
        }
		
		GUI.EndScrollView ();

		if (GUI.Button (new Rect (Screen.width * .4f, Screen.height * .75f,
		                          Screen.width * .2f, Screen.height * .1f), "Back")) {
			ResetFlags();
            _showEquipment = true;
		}

	}

	private void ShowStatus() {
		GUI.Label (new Rect (Screen.width * .1f, Screen.height * .1f, Screen.width * .6f, Screen.height * .1f),
                   string.Format("HP: {0}/{1}", _clientSettings.Health, _clientSettings.MaxHealth), labelStyle);
		GUI.Label (new Rect (Screen.width * .1f, Screen.height * .2f, Screen.width * .6f, Screen.height * .1f),
                   string.Format("MP: {0}/{1}", _clientSettings.Energy, _clientSettings.MaxEnergy), labelStyle);
        GUI.Label(new Rect(Screen.width * .1f, Screen.height * .3f, Screen.width * .6f, Screen.height * .1f),
                   string.Format("Gold: {0}", _clientSettings.Gold), labelStyle);
        GUI.Label(new Rect(Screen.width * .1f, Screen.height * .4f, Screen.width * .6f, Screen.height * .1f),
                   string.Format("Level: {0}", _clientSettings.CharacterLevel, _clientSettings.MaxHealth), labelStyle);
        GUI.Label(new Rect(Screen.width * .1f, Screen.height * .5f, Screen.width * .6f, Screen.height * .1f),
                   string.Format("Experience: {0}/{1}", _clientSettings.Experience, _clientSettings.NextLevel), labelStyle);
		
	}

    private void ShowCredits()
    {
        List<string> credits = new List<string>() {
            "Project Lead - Jeff",
            "Artistic Director - Sarah",
            "Additional Graphics - Stephen \"Redshrike\" Challener",
            "Combat Mechanics - Andrew",
            "Music & Audio - Alex",
        };

        int rowCount = credits.Count + 1;
        scrollPosition = GUI.BeginScrollView(new Rect(Screen.width * .1f, 10f, Screen.width * .65f, Screen.height * .8f), 
            scrollPosition,
            new Rect(0, 0, Screen.width * .6f, rowCount * Screen.height * .1f));

        int creditRow = 0;
        foreach (string credit in credits)
        {

            GUI.Label(new Rect(0 * Screen.width * .6f, creditRow * Screen.height * .1f,
                                      Screen.width * .5f, Screen.height * .1f), credit, labelStyle);

            creditRow++;
        }

        GUI.EndScrollView();
    }

   

	private void ShowDetails() {
        if (_showItems)
        {
            InventoryItem item = _clientSettings.Inventory.First(i => i.AbilityID == _chosenAbility);

            GUI.Box(_detailTextRect, string.Empty);
            GUI.Label(_detailTextRect, string.Format("Item: {0}\nDescription: {1}", item.Name, item.Description), labelStyle);
        }
        else if (_showQuests)
        {
            Quest quest = _characterQuests.First(q => q.QuestID == _chosenAbility);

            GUI.Box(_detailTextRect, string.Empty);
            GUI.Label(_detailTextRect, string.Format("Quest: {0}\n{1}", quest.QuestDescription, quest.StatusDescription), labelStyle);
        }
        else if (_spendSkillPoints)
        {
            Ability chosenAbility = _characterTypeAbilities.First(s => s.AbilityID == _chosenAbility);
            Ability currentAbility = _clientSettings.Spells.FirstOrDefault(s => s.AbilityID == _chosenAbility);

            string description = string.Format("Ability: {0}\nDescription: {1}", chosenAbility.Name, chosenAbility.Description);

            if (currentAbility == null || currentAbility.Level < currentAbility.MaxLevel)
            {
                GUI.Box(_detailTextRect, string.Empty);
                GUI.Label(_detailTextRect, description);

                if (_learningSkill)
                {
                    GUI.Label(_detailTextRect, string.Format("{0}\nLEARNING....", description), labelStyle);
                }
                else if (_clientSettings.SkillPoints <= 0)
                {
                    GUI.Label(_detailTextRect, string.Format("{0}\nNot enough skill points....", description), labelStyle);
                }
                else if (GUI.Button(new Rect(Screen.width * .3f, Screen.height * .75f, Screen.width * .2f, Screen.height * .1f), "LEARN", buttonStyle))
                {
                    GameServer.LearnSkill(_chosenAbility);
                    _learningSkill = true;

                    if (currentAbility == null)
                        _clientSettings.Spells.Add(Ability.GetAbility(_chosenAbility, 1));
                    else
                        currentAbility.Level++;

                    _clientSettings.SkillPoints--;
                }
            }
            else
            {
                GUI.Label(_detailTextRect, string.Format("{0}\nYou are at the max level", description), labelStyle);
            }
        }
        else if (_showAbilities)
        {
            Ability ability = _clientSettings.Spells.First(s => s.AbilityID == _chosenAbility);

            GUI.Label(_detailTextRect, string.Format("Ability: {0}\nDescription: {1}", ability.Name, ability.Description), labelStyle);
        }
	}

	private void ShowQuit() {
		// Make a background box
		GUI.Box (new Rect (10.DpToPixel(), 10.DpToPixel(), 200.DpToPixel(),300.DpToPixel()), "Are you sure you want to quit?", labelStyle);
		if (GUI.Button (new Rect (40.DpToPixel(), 40.DpToPixel(), 100.DpToPixel(), 100.DpToPixel()), "Yes")) {
            GameServer.SendLogout();
			Application.Quit();
		}
		if (GUI.Button (new Rect (40.DpToPixel(), 160.DpToPixel(), 100.DpToPixel(), 100.DpToPixel()), "No")) {
			_showQuit=false;
		}
	}

	private void ResetFlags() {
		scrollPosition = Vector2.zero;

        _chooseLeftHand = false;
        _chooseRightHand = false;
        _chooseBody = false;
        _chooseHead = false;
        _chooseRanged = false;

        _showItems = false;
        _showQuests = false;
		_showAbilities = false;
        _spendSkillPoints = false;
		_showEquipment = false;
		_showEquipmentSelection = false;
		_showStatus = false;
	    _showCredits = false;
		_showQuit = false;
		_chooseTarget = false;
	}
}
