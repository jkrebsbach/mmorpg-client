﻿using Assets.Settings.Scripts;
using UnityEngine;
using System.Collections;
using System;
using Assets.Network;
#if UNITY_WP8 || UNITY_METRO
#else
using System.Net;
#endif

public class MainMenu : MonoBehaviour {
	int serverInt = 0; // Azure
    //int serverInt = 1;  // Localhost

	public GameObject[] prefabs;
	private ClientSettings _clientSettings;

	private bool _loggedIn;
    private bool _loggingIn = false;
    private bool _thirdParty = false;
	private bool _connecting;
	private string _username = string.Empty;
	private string _password = string.Empty;
    private AuthEnum _authEnum;
    private AuthStatus _authStatus;
    private string _passwordHash = string.Empty;
    private long _currentTimezoneOffset;
    private Guid _thirdPartyToken;
	private Guid _loginToken;
	private DateTime _whoAmICheck = DateTime.MinValue;

    private string _statusMessage = string.Empty;
    
	private const string LOGIN_URL = "https://newmoonlegacy.azurewebsites.net/Account/LoginExternal?email={0}&password={1}";
    private const string VALIDATE_URL = "https://newmoonlegacy.azurewebsites.net/Account/ValidateHash?email={0}&hash={1}";
    //private const string LOGIN_URL = "http://localhost:58642/Account/LoginExternal?email={0}&password={1}";
    private const string GOOGLE_LOGIN_URL = "https://newmoonlegacy.azurewebsites.net/Account/UnityExternalLogin?provider=Google&returnUrl=/Account/GoogleExternalSignOn&guid={0}";
    private const string THIRD_PARTY_URL = "https://newmoonlegacy.azurewebsites.net/Account/CheckThirdParty?guid={0}";
    
    private string[] servers = new string[] { "", "192.168.0.38" };
    private string[] serverNames = new string[] { "AZURE", "LOCALHOST(TEST)" };

    private string remoteServerHostname = "robotavenger.cloudapp.net";
    //private string remoteServerHostname = "192.168.0.38";

    private int serverPort = 8690;
    private int webSocketServerPort = 8960;

    private bool _mute;
    public Texture2D muteButton;
    public Texture2D unmuteButton;
    public GUIStyle bannerStyle = new GUIStyle();
    public GUIStyle promptStyle = new GUIStyle();

    private GUIStyle labelStyle = null;
    private GUIStyle textStyle = null;
    private GUIStyle buttonStyle = null;
    private GUIStyle toggleStyle = null;
    private bool _skinInitialized = false;

    private enum AuthEnum 
    {
        UsernamePassword,
        Google
    }

    private enum AuthStatus
    {
        CachedLogin,
        UsernamePassword,
        LoggingIn
    }

    // Use this for initialization
	void Start () {
		// Start the game manager (Client Settings) as soon as the app opens
		Instantiate(prefabs[0]);
		_clientSettings = FindObjectOfType<ClientSettings> ();

        PlayerPrefs.DeleteKey("PlayerSceneX");
        PlayerPrefs.DeleteKey("PlayerSceneY");

        if (PlayerPrefs.HasKey("Mute"))
            _mute = (PlayerPrefs.GetInt("Mute") == 1);
        else
        {
            _clientSettings.Mute = false;
            _mute = false;
        }

        _authStatus = AuthStatus.UsernamePassword;
        if (PlayerPrefs.HasKey("Password") && PlayerPrefs.HasKey("User"))
        {
            _username = PlayerPrefs.GetString("User");
            _password = PlayerPrefs.GetString("Password");
            _passwordHash = _password;
            
            if (PlayerPrefs.HasKey("AuthEnum"))
            {
                _authEnum = (AuthEnum) PlayerPrefs.GetInt("AuthEnum");
                _authStatus = AuthStatus.CachedLogin;
            }
        }

        AudioListener.pause = _mute;

        #if UNITY_WP8 || UNITY_METRO || UNITY_WEBGL
        servers[0] = remoteServerHostname;
        #else
        servers[0] = Dns.GetHostEntry(remoteServerHostname).AddressList[0].ToString();
        #endif

	    _loggedIn = false;
		_connecting = false;
	}

    private bool _mobileScreenSize = false;
    private Vector2 _scrollPosition = Vector2.zero;

    // Update is called once per frame
    void OnGUI () {
        if (!_skinInitialized)
        {
            labelStyle = GUI.skin.label;
            buttonStyle = GUI.skin.button;
            textStyle = GUI.skin.textArea;
            toggleStyle = GUI.skin.toggle;

            promptStyle.fontSize = 30.DpToPixel();

            labelStyle.fontSize = 20.DpToPixel();
            buttonStyle.fontSize = 20.DpToPixel();
            textStyle.fontSize = 20.DpToPixel();
            toggleStyle.fontSize = 20.DpToPixel();
            
            // Custom formatting for small screen form factors
            if (DisplayMetricsUtil.ScreenSizeDpUnit.width < 900 || DisplayMetricsUtil.ScreenSizeDpUnit.height < 900)
            {
                _mobileScreenSize = true;

                GUI.skin.verticalScrollbar.fixedWidth = Screen.width * 0.1f;
                GUI.skin.verticalScrollbarThumb.fixedWidth = Screen.width * 0.1f;

                GUI.skin.horizontalScrollbar.fixedWidth = Screen.height * 0.5f;
                GUI.skin.horizontalScrollbarThumb.fixedWidth = Screen.height * 0.5f;

                textStyle.wordWrap = false;
                textStyle.clipping = TextClipping.Clip;
            }

            _skinInitialized = true;
        }
        
        if (_statusMessage != string.Empty)
        {
			GUI.Label (new Rect (350, 550, 450, 50), _statusMessage, promptStyle);
		}

        if (GUI.Button(new Rect(10, 10, 50, 50), _mute ? unmuteButton : muteButton))
        {
            _mute = !_mute;
            AudioListener.pause = _mute;
            _clientSettings.Mute = _mute;
        }

        // Web Client should auto-login with Test user account
#if UNITY_WEBPLAYER || UNITY_WEBGL

        if (!_loggingIn)
        {
            _username = "test@test.com";
            _password = "Pa$$word1";
            _loggingIn = true;

            _statusMessage = "AUTHENTICATING...";
            StartCoroutine(LoginUser(_username, _password));
        }

#else
        // TEST - Diagnostic for determining screen resolutions
        //GUI.Label(new Rect(200, 150, Screen.width - 300, 50), Screen.dpi.ToString(), bannerStyle);
        //GUI.Label(new Rect(200, 350, Screen.width - 300, 50), DisplayMetricsUtil.ScreenSizeDpUnit.width + " - " + DisplayMetricsUtil.ScreenSizeDpUnit.height, bannerStyle);
        	
        
        Rect cachedLoginButton = new Rect(200.DpToPixel(), 250.DpToPixel(), 200.DpToPixel(), 75.DpToPixel());
        Rect changeUserButton = new Rect(200.DpToPixel(), 450.DpToPixel(), 200.DpToPixel(), 75.DpToPixel());

        Rect emailLabel = new Rect(50.DpToPixel(), 150.DpToPixel(), 200, 100.DpToPixel());
        Rect emailTextArea = new Rect(50.DpToPixel(), 190.DpToPixel(), 400.DpToPixel(), 60.DpToPixel());
        Rect passwordLabel = new Rect(50.DpToPixel(), 250.DpToPixel(), 200, 100.DpToPixel());
        Rect passwordTextArea = new Rect(50.DpToPixel(), 300.DpToPixel(), 400.DpToPixel(), 60.DpToPixel());
        Rect createAcctButton = new Rect(100.DpToPixel(), 400.DpToPixel(), 200.DpToPixel(), 75.DpToPixel());
        Rect signOnButton = new Rect(350.DpToPixel(), 400.DpToPixel(), 150.DpToPixel(), 75.DpToPixel());

        Rect alternateLabel = new Rect(200.DpToPixel(), 500.DpToPixel(), 200, 75);

        if (_mobileScreenSize)
	    {
            cachedLoginButton = new Rect(100.DpToPixel(), 250.DpToPixel(), 200.DpToPixel(), 75.DpToPixel());
            changeUserButton = new Rect(100.DpToPixel(), 450.DpToPixel(), 200.DpToPixel(), 75.DpToPixel());

            emailLabel = new Rect(50.DpToPixel(), 75.DpToPixel(), 200, 100.DpToPixel());
            emailTextArea = new Rect(50.DpToPixel(), 110.DpToPixel(), 400.DpToPixel(), 60.DpToPixel());
            passwordLabel = new Rect(50.DpToPixel(), 175.DpToPixel(), 200, 100.DpToPixel());
            passwordTextArea = new Rect(50.DpToPixel(), 225.DpToPixel(), 400.DpToPixel(), 60.DpToPixel());
            createAcctButton = new Rect(50.DpToPixel(), 350.DpToPixel(), 150.DpToPixel(), 75.DpToPixel());
            signOnButton = new Rect(225.DpToPixel(), 350.DpToPixel(), 150.DpToPixel(), 75.DpToPixel());
            alternateLabel = new Rect(50.DpToPixel(), 450.DpToPixel(), 200, 75);
        }

        _scrollPosition = GUI.BeginScrollView(new Rect(0, 0, Screen.width, Screen.height), _scrollPosition,
            new Rect(0, 0, 450.DpToPixel(), 700.DpToPixel()));

        GUI.Label(new Rect(200, 25, Screen.width - 300, 50), "NEW MOON", bannerStyle);

        if (GUI.Button(new Rect(10, 620.DpToPixel(), 100.DpToPixel(), 75.DpToPixel()), "QUIT"))
        {
            Application.Quit();
        }


        if (!_loggedIn && !_loggingIn)
        {
	        if (_authStatus == AuthStatus.CachedLogin)
	        {
	            GUI.Label (new Rect (50, 150.DpToPixel(), 300, 75), _username, promptStyle);

                if (GUI.Button(cachedLoginButton, "LOGIN"))
                {
                    if (_authEnum == AuthEnum.UsernamePassword)
                        LoginUser();
                    else
                        LoginGoogle();
                }
                if (GUI.Button(changeUserButton, "CHANGE USER"))
                {
                    _authStatus = AuthStatus.UsernamePassword;
                }
	        } else if (_authStatus == AuthStatus.UsernamePassword) {
	            GUI.Label(emailLabel, "Email", promptStyle);
	            GUI.Label(passwordLabel, "Password", promptStyle);

	            _username = GUI.TextArea(emailTextArea, _username, textStyle);
	            _password = GUI.PasswordField(passwordTextArea, _password, '*', textStyle);

                if (GUI.Button(createAcctButton, "New User"))
                {
                    Application.OpenURL("https://newmoonlegacy.azurewebsites.net/Account/Register");
                }
                if (GUI.Button(signOnButton, "LOGIN"))
                {
                    LoginUser();
                }

                GUI.Label(alternateLabel, "- or sign in using -", promptStyle);
                if (GUI.Button(new Rect(225.DpToPixel(), 550.DpToPixel(), 150.DpToPixel(), 75.DpToPixel()), "GOOGLE"))
                {
                    LoginGoogle();
                }

                
                
		    } 
	    }
      
        GUI.EndScrollView();  
#endif

        if (_loggedIn) {
			if (!_connecting) {	
				string serverIP = servers[serverInt];
				_statusMessage = "OPENING CONNECTION...";

			    _clientSettings.CurrentTimezoneOffset = _currentTimezoneOffset;
                var messageProcessor = new MMORPGMessaging();
			    MMORPGMessaging.ClientSettings = _clientSettings;

                StartCoroutine(_clientSettings.Connect(
                    serverIP, serverPort, webSocketServerPort, 
                    _username, _loginToken.ToString(), messageProcessor));
				_connecting = true;
			} else {
				_statusMessage = "SIGNING IN...";

			    if (_clientSettings.IsConnected)
			    {
			        if (_clientSettings.Locations.Count == 0 && _whoAmICheck < _clientSettings.ServerTime)
			        {
			            GameServer.WhoAmI();
			            _whoAmICheck = _clientSettings.ServerTime.AddSeconds(2);
			        }
			        else if (_clientSettings.LoginFailure)
			        {
			            _statusMessage = "Having problems logging in.  Please try again later.";
			            _connecting = false;
			            _loggedIn = false;
			            _loggingIn = false;
			        }
			        else if (_clientSettings.WhoAmI != Guid.Empty && _clientSettings.Locations.Count > 0)
			        {
			            string resourceName = string.Empty;
			            switch (_clientSettings.CharacterTypeID)
			            {
			                case 1:
			                    resourceName = "woodsman";
			                    break;
			                case 2:
			                    resourceName = "merchant";
			                    break;
			                default:
			                    resourceName = "battlemage";
			                    break;
			            }

			            // Read these from the WhoAmI initialization
			            PlayerPrefs.SetFloat("PlayerX", _clientSettings.LoginX);
			            PlayerPrefs.SetFloat("PlayerY", _clientSettings.LoginY);
			            PlayerPrefs.SetString("resourceName", resourceName);

			            PlayerPrefs.SetString("User", _username);
			            PlayerPrefs.SetString("Password", _passwordHash);
                        PlayerPrefs.SetInt("AuthEnum", (int)_authEnum);
			            
			            PlayerPrefs.Save();

			            if (_clientSettings.CurrentLocation > 0)
			            {
			                foreach (var location in _clientSettings.Locations)
			                {
			                    if (_clientSettings.CurrentLocation == location.LocationID)
			                    {
			                        PlayerPrefs.SetString("locationResource", location.Resource);
			                        PlayerPrefs.SetFloat("PlayerSceneX", location.PlayerStart.x);
			                        PlayerPrefs.SetFloat("PlayerSceneY", location.PlayerStart.y);
			                    }
			                }

			                Application.LoadLevel("LocationScene");
			            }
			            else
			                Application.LoadLevel("OverworldScene");

			        }
			    }
			}
        }
    }

    void Update()
    {
        if (_thirdParty && _whoAmICheck < DateTime.Now)
        {
            _whoAmICheck = DateTime.Now.AddSeconds(2);

            StartCoroutine(CheckThirdPartyLogin());
        }
    }

    private void LoginUser()
    {
        _authEnum = AuthEnum.UsernamePassword;

        if (!_loggingIn && _username != string.Empty && _password != string.Empty)
        {
            _loggingIn = true;

            _statusMessage = "AUTHENTICATING...";
            StartCoroutine(LoginUser(_username, _password));
        }
    }

    private void LoginGoogle()
    {
        _authEnum = AuthEnum.Google;

        if (_authStatus == AuthStatus.CachedLogin)
        {
            _thirdPartyToken = new Guid(_passwordHash);
            _thirdParty = true;
        }
        else
        {
            _thirdPartyToken = Guid.NewGuid();
            _passwordHash = _thirdPartyToken.ToString();
            Application.OpenURL(String.Format(GOOGLE_LOGIN_URL, _thirdPartyToken));
            _thirdParty = true;
        }
    }

    private IEnumerator CheckThirdPartyLogin() {
        string loginPath = string.Empty;
        WWW loginRequest = null;
        try
        {
            loginPath = string.Format(THIRD_PARTY_URL, _thirdPartyToken);
            loginRequest = new WWW(loginPath);
        }
        catch (Exception ex)
        {
            _loggedIn = false;
            _loggingIn = false;
            _connecting = false;

            _statusMessage = ex.Message;

        }

        yield return loginRequest;

        try
        {
            if (loginRequest.error != null)
            {
                _statusMessage = loginRequest.error;
                _loggedIn = false;
                _loggingIn = false;
                _connecting = false;
            }
            else
            {
                JSONObject j = new JSONObject(loginRequest.text);
                string strSuccess = j["Success"].str;
                bool success = bool.Parse(strSuccess);
                _username = j["Email"].str;

                if (success)
                {
                    _loginToken = new Guid(j["Token"].str);
                    long serverTime = long.Parse(j["CurrentTime"].str);
                    _currentTimezoneOffset = serverTime - DateTime.UtcNow.Ticks;
                    _loggedIn = true;
                }
                else
                {
                    // Busy wait while we wait for OpenAuth to finish
                }
            }
        }
        catch (Exception ex)
        {
            _loggedIn = false;
            _loggingIn = false;
            _connecting = false;

            _statusMessage = ex.Message;
        }

    }

	private IEnumerator LoginUser(string username, string password) {
        WWW loginRequest = null;
		try
		{
		    string loginPath;
		    if (_password == _passwordHash)
                loginPath = string.Format(VALIDATE_URL, WWW.EscapeURL(username), WWW.EscapeURL(_passwordHash)).Replace(" ", "%20");
            else
                loginPath = string.Format(LOGIN_URL, WWW.EscapeURL(username), WWW.EscapeURL(password)).Replace(" ", "%20");
			loginRequest = new WWW(loginPath);
		}
		catch(Exception ex)
		{
            _loggedIn = false;
            _loggingIn = false;
            _connecting = false;

			_statusMessage = ex.Message;

		}

        yield return loginRequest;

        try
        {
            if (loginRequest.error != null)
            {
                _statusMessage = loginRequest.error;
                _loggedIn = false;
                _loggingIn = false;
                _connecting = false;
            }
            else
            {
                JSONObject j = new JSONObject(loginRequest.text);
                string strSuccess = j["Success"].str;
                bool success = bool.Parse(strSuccess);

                if (success)
                {
                    _loginToken = new Guid(j["Token"].str);
                    _passwordHash = j["Hash"].str;
                    long serverTime = long.Parse(j["CurrentTime"].str);
                    _currentTimezoneOffset = (long)serverTime - DateTime.UtcNow.Ticks;
                    _loggedIn = true;
                }
                else
                {
                    _statusMessage = j["Token"].str;
                    _loggedIn = false;
                    _loggingIn = false;
                    _connecting = false;
                }
            }
		}
		catch(Exception ex)
		{
            _loggedIn = false;
            _loggingIn = false;
            _connecting = false;

			_statusMessage = ex.Message;
		}

	}
}