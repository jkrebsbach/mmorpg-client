﻿public abstract class ProcessMessageBase {

    public abstract void ProcessServerNetworkMessage(ServerNetworkMessage message);

	// Server Settings
	public static ClientSettings ClientSettings { get; set; }
}
