﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class ScreenSlashes : MonoBehaviour
{
    public Sprite FirstSlashSprite;
    public Sprite SecondSlashSprite;

    private SpriteRenderer _firstSlash;
    private SpriteRenderer _secondSlash;

    // Use this for initialization
    void Start()
    {
        _firstSlash = GameObject.Find("FirstSlash").GetComponent<SpriteRenderer>();
        _secondSlash = GameObject.Find("SecondSlash").GetComponent<SpriteRenderer>();

        _firstSlash.sprite = FirstSlashSprite;
        _secondSlash.sprite = SecondSlashSprite;

        _firstSlash.enabled = false;
        _secondSlash.enabled = false;

        StartCoroutine(slashAnimation());
    }

    IEnumerator slashAnimation()
    {
        yield return new WaitForSeconds(.1f);

        if (_firstSlash != null)
            _firstSlash.enabled = true;
        
        yield return new WaitForSeconds(.3f);

        if (_secondSlash != null)
            _secondSlash.enabled = true;
    }

}
