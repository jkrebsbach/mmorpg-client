/// <summary>
/// Battle menu.
/// Attached to Main Camera
/// </summary>
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Linq;

public class BattleMenu : MonoBehaviour {

	public Texture2D backgroundTexture;

	private Guid _battleID;
	private bool _abilityScreen = false;
	private bool _itemScreen = false;
	private bool _attemptEscape = false;
    private bool _switchRow = false;
	private bool _attemptDefend = false;
	private bool _chooseTarget = false;
	private bool _useAttack = false;
	private bool _useInventory = false;
	private int _chosenAbility = 0;
	private DateTime _coolDown = DateTime.MinValue;
	private ClientSettings _clientSettings = null;
	private ProgressBar _progressBar = null;
    private AudioSource _audioSource = null;

	private GUIStats _guiStats = null;
	private GameObjectSettings _clientAttributes = null;
	private List<GameObjectSettings> _gameObjects = new List<GameObjectSettings>();
    private AnimationController _animationController;
    
    public AudioClip AttackClip;
    public AudioClip VictoryClip;
    public GameObject RotateWeaponPrefab = null;
    public GameObject SlashEffectPrefab = null;
    public GameObject DestroyEnemyPrefab = null;
    public GameObject GenericAbilityPrefab = null;
	public Vector2 abilityScrollPosition = Vector2.zero;
    public bool _rewardDialog = false;

    protected void Start()
    {
        _clientSettings = FindObjectOfType<ClientSettings>();
        _audioSource = FindObjectOfType<AudioSource>();
        _progressBar = FindObjectOfType<ProgressBar>();
        
        GameObject animation = GameObject.Find("AnimationController");
        _animationController = animation.GetComponent<AnimationController>();

        _animationController.AttachEffect(GenericAbilityPrefab);

        _battleID = new Guid(PlayerPrefs.GetString("battleID"));

        if (_clientSettings.CurrentLocation == 4) // Forest
            LoadBackground("ForestScene");
        else if (_clientSettings.CurrentLocation == 5) // Cave
            LoadBackground("MountainScene");
        else
            LoadBackground("PlainsScene");

        // Initialize progress bar to be top center of screen
        _progressBar.pos = new Vector2(Screen.width * .2f, Screen.height * .15f);
        _progressBar.size = new Vector2(Screen.width * .6f, Screen.height * .1f);

        _progressBar.InitializeClientSettings(_clientSettings);

        _guiStats = Camera.main.GetComponent<GUIStats>();

        GameServer.EnterBattle(_battleID);
    }

    public IEnumerable<GameObjectSettings> BattleEnemies
	{
		get
		{
			return _gameObjects.Where (go => !go.Ally);
		}
	}
	
	public IEnumerable<GameObjectSettings> BattleAllies
	{
		get
		{
			return _gameObjects.Where (go => go.Ally);
		}
	}

	public GameObjectSettings GetObjectFromID(Guid guid)
	{
		GameObjectSettings g = _gameObjects.Find(
			delegate(GameObjectSettings gos)
			{
			return gos.GameObjectID == guid;
		}
		);
		return g;
	}

	public void UpdateBattleEntity(string[] msg)
	{	
		Guid guid = new Guid (msg [1]);
		Int32 health = Int32.Parse (msg [2]);
		Int32 maxHealth = Int32.Parse (msg [3]);
		Int32 energy = Int32.Parse (msg [4]);
		Int32 maxEnergy = Int32.Parse (msg [5]);
		String resourceName = msg [6];
		Int32 index = Int32.Parse (msg [7]);
		bool ally = Int32.Parse(msg [8]) == 1;
		String characterName = msg [9];
		String cooldown = msg [10];
        bool frontRow = Int32.Parse(msg[11]) == 1;
        String buffString = msg[12];
		
		GameObjectSettings g = GetObjectFromID(guid);
		// Check if we have the player already.
		if (g == null) {
			g = AddGameObject (guid, resourceName, characterName, ally, frontRow, health, maxHealth, energy, maxEnergy, 0);

            if (g.Ally)
            {
                // Ally sprites must be shrunk...
                g.transform.localScale = new Vector3(.5f, .5f);

                StartCoroutine(g.FaceLeft());
            }
        }
        else
        {
            g.UpdateStats(ally, frontRow, health, maxHealth, energy, maxEnergy, resourceName);
		}

        g.ParseBuffString(buffString);
		
		if (_clientSettings.WhoAmI == g.GameObjectID)
		{
            _clientAttributes = g;
			_coolDown = DateTime.Parse(cooldown);
			CoolDownInit();
		}
	}

	private GameObjectSettings AddGameObject(Guid guid, string resourceName, string characterName, bool ally, bool frontRow,
	            int health, int maxHealth, int energy, int maxEnergy, int prefabID)
	{
		GameObject newObject = _clientSettings.RetrieveCharacterPrefab(ally ? 3 : 4);
		
		GameObjectSettings gos = newObject.GetComponent<GameObjectSettings> ();
		gos.Initialize (guid, ally, frontRow, health, maxHealth, energy, maxEnergy, resourceName, characterName);
		
		_gameObjects.Add(gos);
		newObject.name = "["+resourceName+"] "+guid;
		
		gos.Index = _gameObjects.Where (go => go.Ally == ally).Count ();
		
		return gos;
	}

	public IEnumerator DestroyBattleEntity(string[] msg)
	{
		string ID = msg[1];
		GameObjectSettings g = _gameObjects.FirstOrDefault(go => go.GameObjectID == new Guid(ID));

		if(g != null)
		{
            if (g.GameObjectID == _clientSettings.WhoAmI)
            {
                Vector2 screenPos = GetComponent<Camera>().WorldToScreenPoint(g.transform.position);
                Vector2 screenOffset = new Vector2(screenPos.x, Screen.height - screenPos.y);

                g.QueueMessage("DEAD", screenOffset);

                _clientSettings.Death();

                DialogData[] dialogData = new DialogData[1];
                dialogData[0] = new DialogData()
                {
                    Text = "You have died.  Click OK to respawn.",
                    Next = -1
                };

                GameObject newObject = GameServer.ClientSettings.RetrieveCharacterPrefab(6);

                Dialog dialog = newObject.GetComponent<Dialog>();
                dialog.Initialize(dialogData.ToArray(), -1);
            }

            GameObject deathExplode = Instantiate(DestroyEnemyPrefab, Vector3.zero, Quaternion.identity) as GameObject;
            deathExplode.transform.position = g.transform.position;
            Destroy(deathExplode, 10);
            yield return new WaitForSeconds(2.0f);

			GameObject.Destroy(g.gameObject);
			_gameObjects.Remove(g);
			
			int allyIndex = 0;
			int enemyIndex = 0;
			foreach(GameObjectSettings gos in _gameObjects)
			{
				if (gos.Ally)
				{
					allyIndex++;
                    gos.Index = allyIndex;
				}
				else
				{
					enemyIndex++;
                    gos.Index = enemyIndex;
				}
			}
		}

        yield return null;
	}

	public void CoolDownInit()
	{
		_progressBar.ResetBar (_coolDown);
	}

	public void InitiateAbility(string[] msg)
	{
		Guid casterID = new Guid(msg[1]);
		Guid targetID = new Guid(msg[2]);
		int spellID = Int32.Parse(msg[3]);

		GameObjectSettings target = _gameObjects.FirstOrDefault(go => go.GameObjectID == targetID);
        GameObjectSettings gos = GetObjectFromID(casterID);
			
		if (target != null && gos != null) {
			StartCoroutine (gos.AnimateSpell (target, spellID));
		}
	}

	public void UpdateHealth(string[] msg)
	{
		Guid guid = new Guid(msg[1]);
		int healthChange = Int32.Parse (msg[2]);

		GameObjectSettings g = GetObjectFromID(guid);

        if (g != null)
        {
            Vector2 screenPos = GetComponent<Camera>().WorldToScreenPoint(g.transform.position);
            Vector2 screenOffset = new Vector2(screenPos.x, Screen.height - screenPos.y);

            g.UpdateHealth(healthChange, screenOffset);
        }
	}
	
	protected void Update() {
		while( _clientSettings.UpdateBattleEntityQueue.Count != 0) {
			string[] battleEntity = _clientSettings.UpdateBattleEntityQueue.Pop();
			UpdateBattleEntity (battleEntity);
		}
		while( _clientSettings.DeleteBattleEntityQueue.Count != 0) {
			string[] battleEntity = _clientSettings.DeleteBattleEntityQueue.Pop();
			StartCoroutine(DestroyBattleEntity (battleEntity));
		}
		while( _clientSettings.InitiateAbilityQueue.Count != 0) {
			string[] ability = _clientSettings.InitiateAbilityQueue.Pop();
			InitiateAbility (ability);
		}
		while( _clientSettings.HealthQueue.Count != 0) {
			string[] health = _clientSettings.HealthQueue.Pop();
			UpdateHealth (health);
		}
		while (_clientSettings.EscapeAttemptQueue.Count != 0) {
			string[] escape = _clientSettings.EscapeAttemptQueue.Pop ();
			EscapeAttempt(escape);
		}
        while (_clientSettings.BattleRewardQueue.Count != 0)
        {
            string[] reward = _clientSettings.BattleRewardQueue.Pop();
            BattleReward(reward);
        }


        foreach (var gos in _gameObjects)
        {
            float column = (gos.Ally ? 0.7f : 0.1f);
            float ordinal = .6f - ((float)gos.Index * .15f);

            if (!gos.FrontRow)
                column += .05f;

            Vector3 screenPoint = new Vector3(column * Screen.width, ordinal * Screen.height, GetComponent<Camera>().nearClipPlane);
            Vector3 worldPoint = GetComponent<Camera>().ScreenToWorldPoint(screenPoint);

            gos.transform.position = new Vector3(worldPoint.x, worldPoint.y, 0f);
        }
	
	}

	protected void OnGUI() {
        if (!_rewardDialog && _clientAttributes != null)
        {
            RenderAllyStats();

            // When we are waiting for commands, show command buttons
            if (_coolDown < _clientSettings.ServerTime)
            {
                if (_chooseTarget)
                {
                    RenderTargetButtons();
                }
                else if (_abilityScreen)
                {
                    RenderAbilities();
                }
                else if (_itemScreen)
                {
                    RenderItems();
                }
                else if (_attemptEscape)
                {
                    AttemptEscape();
                }
                else if (_switchRow)
                {
                    SwitchRow();
                }
                else if (_attemptDefend)
                {
                    AttemptDefend();
                }
                else
                {
                    // Display our buttons
                    if (GUI.Button(new Rect(Screen.width * .15f, Screen.height * .10f, Screen.width * .2f, Screen.height * .1f), "Attack"))
                    {
                        _chosenAbility = -1;
                        _useAttack = true;
                        _chooseTarget = true;
                    }

                    if (GUI.Button(new Rect(Screen.width * .4f, Screen.height * .10f, Screen.width * .2f, Screen.height * .1f), "Ability"))
                    {
                        _abilityScreen = true;
                    }
                    if (GUI.Button(new Rect(Screen.width * .65f, Screen.height * .10f, Screen.width * .2f, Screen.height * .1f), "Item"))
                    {
                        _itemScreen = true;
                    }

                    if (GUI.Button(new Rect(Screen.width * .3f, Screen.height * .25f, Screen.width * .2f, Screen.height * .1f), "Defend"))
                    {
                        _attemptDefend = true;
                    }

                    if (GUI.Button(new Rect(Screen.width * .55f, Screen.height * .25f, Screen.width * .2f, Screen.height * .1f), "Escape"))
                    {
                        _attemptEscape = true;
                    }

                    if (GUI.Button(new Rect(Screen.width * .55f, Screen.height * .65f, Screen.width * .2f, Screen.height * .1f), _clientAttributes.FrontRow ? "Back Row" : "Front Row"))
                    {
                        _switchRow = true;
                    }
                }
            }
        }
	}

    private void LoadBackground(string backgroundResource)
    {
        GameObject backgroundImage = GameObject.Find("Background Image");
        SpriteRenderer spriteRenderer = backgroundImage.GetComponent<SpriteRenderer>();

        string resourcePath = string.Format("BattleScenes/{0}", backgroundResource);
        spriteRenderer.sprite = Resources.Load<Sprite>(resourcePath);
    }

	private void RenderAbilities() {
        bool hasRanged = (_clientSettings.EquipRanged > 0);
        
        var activeSpells = _clientSettings.Spells.Where(a => a.Active || (hasRanged && a.Type == Ability.AbilityType.Ranged));

        int rowCount = (activeSpells.Count() / 2) + 1;
		abilityScrollPosition = GUI.BeginScrollView(new Rect(Screen.width * .1f, Screen.height * .1f, Screen.width * .8f, Screen.height * .3f), abilityScrollPosition, 
		                                            new Rect(0, 0, Screen.width * .8f, rowCount * Screen.height * .2f));

		int abilityRow = 0;
		int abilityCol = 0;
        foreach (Ability ability in activeSpells)
        {
			GUI.enabled = (_clientAttributes.Energy > ability.Cost[ability.Level - 1]);
			
			if (GUI.Button (new Rect (Screen.width * .1f + abilityCol * Screen.width * .4f, abilityRow * Screen.height * .15f,
			                          Screen.width * .2f, Screen.height * .1f), ability.Name)) {
				_chosenAbility = ability.AbilityID;
				_chooseTarget = true;
			}

			abilityCol++;
			if (abilityCol % 2 == 0) {
				abilityRow++;
				abilityCol = 0;
			}
		}

		GUI.enabled = true; // Can always click back
		if (GUI.Button (new Rect (Screen.width * .1f + abilityCol * Screen.width * .4f, abilityRow * Screen.height * .15f,
		                          Screen.width * .2f, Screen.height * .1f), "Back")) {
			_abilityScreen = false;
		}
		GUI.EndScrollView ();
	}

	private void RenderItems() {
		int rowCount = (_clientSettings.Inventory.Count / 2) + 1;
		abilityScrollPosition = GUI.BeginScrollView(new Rect(Screen.width * .1f, Screen.height * .1f, Screen.width * .8f, Screen.height * .3f), abilityScrollPosition, 
		                                            new Rect(0, 0, Screen.width * .8f, rowCount * Screen.height * .2f));
		
		int itemRow = 0;
		int itemCol = 0;
		foreach (InventoryItem item in _clientSettings.Inventory) {
			if (GUI.Button (new Rect (Screen.width * .1f + itemCol * Screen.width * .4f, itemRow * Screen.height * .15f,
                          Screen.width * .2f, Screen.height * .1f), item.Name + " - " + item.Quantity)) {
					_chosenAbility = item.AbilityID;
					_useInventory = true;
					_chooseTarget = true;
			}

			itemCol++;
			if (itemCol % 2 == 0) {
				itemRow++;
				itemCol = 0;
			}
		}

		if (GUI.Button (new Rect (Screen.width * .1f + itemCol * Screen.width * .4f, itemRow * Screen.height * .15f,
		                          Screen.width * .2f, Screen.height * .1f), "Back")) {
			_itemScreen = false;
		}

		GUI.EndScrollView();
	}

	private void AttemptDefend() {
		// Turn on cooldown - server will calculate correct end time later
		float cooldownDuration = 10f;
        _coolDown = _clientSettings.ServerTime.AddSeconds(cooldownDuration);
		
		CoolDownInit ();
		ResetGUIFlags();

		GameServer.AttemptDefend (_battleID);
	}
	
	private void AttemptEscape() {
        // Turn on cooldown - server will calculate correct end time later
        float cooldownDuration = 10f;
        _coolDown = _clientSettings.ServerTime.AddSeconds(cooldownDuration);

        CoolDownInit();
		ResetGUIFlags();

		GameServer.AttemptEscape (_battleID);
	}

    private void SwitchRow() {
        // Turn on cooldown - server will calculate correct end time later
        float cooldownDuration = 10f;
        _coolDown = _clientSettings.ServerTime.AddSeconds(cooldownDuration);

        CoolDownInit();
        ResetGUIFlags();

        GameServer.SwitchRow(_battleID);
    }

	private void EscapeAttempt(string[] message) {
		bool success = bool.Parse (message [1]);
        if (success)
        {
            if (_clientSettings.CurrentLocation > 0)
                Application.LoadLevel("LocationScene");
            else
                Application.LoadLevel("OverworldScene");
        }
        else
        {
            Vector2 offset = new Vector2(50, 50);
            _guiStats.QueueMessage("Escape failed", offset);
        }
	}

    public void EnterDialog()
    {
        _rewardDialog = true;
    }

    public void ExitDialog()
    {
        if (_clientSettings.CurrentLocation > 0)
            Application.LoadLevel("LocationScene");
        else
            Application.LoadLevel("OverworldScene");

        _rewardDialog = false;
    }

    private void BattleReward(string[] message)
    {
        _audioSource.clip = VictoryClip;
        _audioSource.loop = true;
        _audioSource.Play();

        int experience = 0;
        int gold = 0;
        bool levelUp = false;
        string loot = string.Empty;
        string[] items = new string[0];
            
        if (message.Length > 4)
        {
            experience = int.Parse(message[1]);
            gold = int.Parse(message[2]);
            loot = message[3];
            levelUp = (message[4] == "1");

            if (loot != string.Empty)
                items = loot.Split('~');
        }

        List<DialogData> battleRewards = new List<DialogData>();
        battleRewards.Add(new DialogData()
        {
            Type = DialogData.DialogType.Text,
            Text = string.Format("Got {0} Exp. point(s)", experience),
            Next = battleRewards.Count + 1
        });
        battleRewards.Add(new DialogData()
        {
            Type = DialogData.DialogType.Text,
            Text = string.Format("Got {0} Gold", gold),
            Next = battleRewards.Count + 1
        });
        
        for (int index = 0; index < items.Length; index++)
        {
            int itemID = 0;
            int itemQty = 0;
            string itemName = string.Empty;

            if (items[index].Contains(','))
            {
                itemID = int.Parse(items[index].Split(',')[0]);
                itemQty = int.Parse(items[index].Split(',')[1]);

                itemName = Ability.GetItem(itemID).Name;
            }

            battleRewards.Add(new DialogData()
            {
                Type = DialogData.DialogType.Text,
                Text = string.Format("Enemy dropped {0} ({1})", itemName, itemQty),
                Next = battleRewards.Count + 1
            });
        }

        if (levelUp)
        {
            battleRewards.Add(new DialogData()
            {
                Type = DialogData.DialogType.Text,
                Text = string.Format("Level Up!!!"),
                Next = battleRewards.Count + 1
            });

            _clientSettings.CharacterLevel++;
            _clientSettings.SkillPoints += 1;
        }

        battleRewards[battleRewards.Count - 1].Next = -1;

        _clientSettings.Gold += gold;
        _clientSettings.Experience += experience;

        _coolDown = DateTime.MinValue;
        CoolDownInit();

        GameObject newObject = GameServer.ClientSettings.RetrieveCharacterPrefab(6);

        Dialog dialog = newObject.GetComponent<Dialog>();
        dialog.Initialize(battleRewards.ToArray(), -1);
    }

	private void RenderAllyStats() {
		// Show HP & MP
		Vector2 alliesTopLeft = new Vector2 (Screen.width * .8f, Screen.height * .5f);

		GUI.Box (new Rect (alliesTopLeft.x, alliesTopLeft.y,
		                     Screen.width * .2f, Screen.height * .4f), String.Empty);

		foreach (GameObjectSettings battleAlly in BattleAllies) {
			string health = string.Format("HP - {0}/{1}", battleAlly.Health, battleAlly.MaxHealth);
			string energy = string.Format("MP - {0}/{1}", battleAlly.Energy, battleAlly.MaxEnergy);

			string stats = string.Format ("{0}" + System.Environment.NewLine + "{1}" + System.Environment.NewLine + "{2}", 
			                              battleAlly.CharacterName, health, energy);

			GUI.Label (new Rect (alliesTopLeft.x, alliesTopLeft.y,
			                         Screen.width * .2f, Screen.height * .2f), stats);

			alliesTopLeft.y += Screen.height * .5f;
		}
	}

	private void RenderTargetButtons() {
		// Render battle targets as clickable buttons
		Vector2 targetsTopLeft = new Vector2 (Screen.width * .05f, Screen.height * .5f);
		Vector2 alliesTopLeft = new Vector2 (Screen.width * .7f, Screen.height * .5f);
		
		foreach (GameObjectSettings battleEnemy in BattleEnemies) {
			if (GUI.Button (new Rect (targetsTopLeft.x, targetsTopLeft.y,
			                          Screen.width * .1f, Screen.height * .1f), battleEnemy.CharacterName)) {
				CastAbility(_chosenAbility, battleEnemy.GameObjectID);
			}
			targetsTopLeft.y += Screen.height * .15f;
		}
		foreach (GameObjectSettings battleAlly in BattleAllies) {
			if (GUI.Button (new Rect (alliesTopLeft.x, alliesTopLeft.y,
			                          Screen.width * .1f, Screen.height * .1f), battleAlly.CharacterName)) {
				CastAbility(_chosenAbility, battleAlly.GameObjectID);
			}
			alliesTopLeft.y += Screen.height * .15f;
		}

		if (GUI.Button (new Rect (Screen.width * .5f, Screen.height * .1f,
		                          Screen.width * .2f, Screen.height * .1f), "Back")) {
			_chooseTarget = false;
			_useInventory = false;
		}
	}

	private void ResetGUIFlags() {
		_abilityScreen = false;
		_itemScreen = false;
		_attemptDefend = false;
		_attemptEscape = false;
        _switchRow = false;
		_chooseTarget = false;
		_useInventory = false;
		_useAttack = false;
	}

	private void CastAbility(int ability, Guid target) {
		// Turn on cooldown - server will calculate correct end time later
		float cooldownDuration = 10f;
        _coolDown = _clientSettings.ServerTime.AddSeconds(cooldownDuration);

		CoolDownInit ();
		if (_useAttack)
			GameServer.AttackInitiate (target);
		else if (_useInventory)
			GameServer.InventoryInitiate(ability, target);
		else
			GameServer.SpellInitiate(ability, target);

		ResetGUIFlags();
	}

}
