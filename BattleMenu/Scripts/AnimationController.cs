using UnityEngine;
using System;

public class AnimationController : MonoBehaviour
{
	private GameObject _effect;

	private float _startTime;
	private float _duration = 3f;
	private Vector3 _currentTarget;

	protected void Start()
	{
	}

	protected void Update()
	{
        if (_effect != null && _currentTarget != Vector3.zero)
        {
			if (transform.position != _currentTarget) {
                _effect.GetComponent<Renderer>().enabled = true;
				transform.position = Vector3.Lerp (transform.position, _currentTarget, (Time.time - _startTime) / _duration);
			} else {
                _effect.GetComponent<Renderer>().enabled = false;
				_currentTarget = Vector3.zero;
			}
		}
	}

    public void AttachEffect(GameObject effect)
    {
        GameObject genericAbility = (GameObject)Instantiate(effect);
        genericAbility.GetComponent<Renderer>().enabled = false;
        genericAbility.transform.parent = transform;

        _effect = genericAbility;

}

	public void SpellAnimation (GameObjectSettings caster, GameObjectSettings target, int abilityID)
	{
		_startTime = Time.time;
		transform.position = caster.transform.position;

		_currentTarget = target.transform.position;

		// Switch statement on ability to set appropriate image/animation
	}

}
