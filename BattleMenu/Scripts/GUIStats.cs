﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GUIStats : MonoBehaviour {

	//Holds the messages being displayed 
	private List<Message> messages = new List<Message>();

	//Internally used to space the messages in time
	private float lastTime = 0f;
	//The screen Y coordinate that causes a fade
	public float fadeY = 300f;
	//The amount of time before fading
	public float fadeTime = 10f;
	//The position that the bottom message starts at 
	public Vector2 messageStartPosition = new Vector2(10,400);
	//The amount of pixels the message scrolls per second
	public float movePerSecond = 40f;
	//The minimum amount of time between messages
	public float spacingTime = 1f;
	//The vertical spacing of messages
	public float verticalSpacing = 25f;
	
	//Static constructor, fixes the start Y position
	public GUIStats() {
		messageStartPosition.y -= messageStartPosition.y % verticalSpacing;
	}
	
	//Call this to display a message
	public void QueueMessage(String message, Vector2 messageOffset) 
	{
		Vector2 messagePosition = 
			new Vector2(messageStartPosition.x, 
			            messageStartPosition.y - (messageStartPosition.y % verticalSpacing));
		Message m = new Message(message, messagePosition, fadeY, fadeTime, messageOffset);

		m.startTime = Time.time;
		messages.Add (m);
	}
	
	//Draws the current messages
	public void Display() {
		foreach(var m in messages) {
			m.Draw();
		}
			
		//If the message has become invisible, delete it
		while (messages.Any (m => !m.visible)) {
			messages.Remove(messages.First (m => !m.visible));
		}

		//Check if we have a new message and the requisite amount of time
		//has passed
		if(Time.time - lastTime > spacingTime) {
			//See if we are displaying messages
			if(messages.Count > 0) {
				//Is there a message in the bottom slot?
				if( Mathf.Floor((messages[0].position.y + verticalSpacing-1) / verticalSpacing)
				   == Mathf.Floor(messageStartPosition.y / verticalSpacing)) {
					//Move the bottom message
					messages[0].position.y -= Time.deltaTime * movePerSecond;
					//Reposition the other messages
					for(var i = 1; i < messages.Count; i++) {
						messages[i].position.y = messages[i-1].position.y - verticalSpacing;
					}
					//Return to stop a new message being added
					return;
				}
			}
			lastTime = Time.time;
		}
			
			
	}

	protected void Start() {
		//QueueMessage("Hello", new Vector2(50, 100));
		//QueueMessage("World", new Vector2(50, 100));
		//QueueMessage("From", new Vector2(50, 100));
		//QueueMessage("1,0.4,0.5:WhyDoIDoIt|0.3,0.4,1: Can you tell me?|1,1,1: Thought not...", new Vector2(50, 100));
	}
	
	protected void OnGUI() {
		//Choose your skin of styles for Label here
		
		//Only update when we are painting
		if(Event.current.type == EventType.Repaint)
			Display();
	}

	public void UpdateHealth(int health, Vector2 messageOffset) {
        if (health == 0) // Miss - maybe gray?
		    QueueMessage("1,0.4,0.5:MISS!!!", messageOffset);
        else if (health < 0) // Heal - maybe green?
            QueueMessage("0.4,0.2,0.7:Health- " + health.ToString(), messageOffset);
        else // Hurt - red?
            QueueMessage("1,0.4,0.5:Health- " + health.ToString(), messageOffset);
	}
	public void SetMaxHealth(int maxHealth, Vector2 messageOffset) {
		QueueMessage("1,0.4,0.5:MaxHealth|0.3,0.4,1:" + maxHealth.ToString(), messageOffset);
	}
	public void UpdateEnergy(int energy, Vector2 messageOffset) {
		QueueMessage("Energy- " + energy.ToString(), messageOffset);
	}
	public void SetMaxEnergy(int maxEnergy, Vector2 messageOffset) {
		QueueMessage("1,0.4,0.5:MaxEnergy|0.3,0.4,1:" + maxEnergy.ToString(), messageOffset);
	}

	internal class Message {
		//Current position of this message
		internal Vector2 position;

		// Current position of the game object
		private Vector2 gameObjectPosition;

		//This message text
		private String message;
		//Current alpha value 
		private float alpha = 0f;
		//Is the message visible?
		internal bool visible = true;
		//The time the message was displayed
		internal float startTime;
		
		//The screen Y coordinate that causes a fade
		private float fadeY = 300f;
		private float fadeTime = 300f;
		
		
		public Message(string strMessage, Vector2 messagePosition, float guiFadeY, float guiFadeTime, Vector2 objectPosition)
		{
			message = strMessage;
			position = messagePosition;
			fadeY = guiFadeY;
			fadeTime = guiFadeTime;

			gameObjectPosition = objectPosition;
		}
		
		//Draw this single message
		internal void Draw() {
			//Store the current GUI color
			var color = GUI.color;
			//adjust for alpha fading
			GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, alpha * color.a);
			//Draw the actual message
			DrawGuiString(message, position + gameObjectPosition);
			//Reset the color
			GUI.color = color;
			
			//Is it time for a fade?
			if(position.y < fadeY || (Time.time - startTime > fadeTime)  ){
				//Fade out      
				alpha = Mathf.Clamp01(alpha - Time.deltaTime * 0.25f);
				if(alpha == 0)
					visible = false;
			}
			else {
				//Fade in
				alpha = Mathf.Clamp01(alpha + Time.deltaTime * 5);
			}
		}
		
		//A section of coloured string!
		class ColorPair
		{
			public String text;
			public Color color;
		}
		
		//Call this to draw a coloured string somewhere from an OnGUI
		private void DrawGuiString(String guiString, Vector2 position) {
			//Do we have anything fancy?
			if(guiString.IndexOf(":") == -1) {
				//No? Then draw a standard label
				GUI.Label(new Rect(position.x, position.y,1000,1000), guiString);
				return;
			}
			//Save the colour
			var color = GUI.color;
			
			//Create an array of string parts - first split on |
			List<ColorPair> labels = new List<ColorPair>();
			
			foreach(var s in guiString.Split(new String[1] {"|"}, System.StringSplitOptions.None))
			{
				//Now split the colour from the text on the :
				var parts = s.Split(new String[1] {":"}, System.StringSplitOptions.None);
				//Extract the color elements, by splitting on "," and make an array of floats
				List<float> colorParts = new List<float>();
				
				foreach(var c in parts[0].Split(new String[1] {","}, System.StringSplitOptions.None))
				{
					//Convert it to a float
					colorParts.Add(Convert.ToSingle(c));
				}
				var np = new ColorPair();
				np.color = new Color(colorParts[0], colorParts[1], colorParts[2], color.a);
				np.text = parts[1];
				
				labels.Add(np);
			}	
			//Draw the sections
			foreach(var label in labels) {
				//Current color (already adjusted for alpha)
				GUI.color = label.color;
				//Draw the string section
				GUI.Label(new Rect(position.x, position.y, Screen.width,100), label.text);
				//Move the X by the width of what we just drew
				position.x += GUI.skin.GetStyle("Label").CalcSize(new GUIContent(label.text)).x + 2;
			}
			//Put the colour back
			GUI.color = color;
		}
	}
}
