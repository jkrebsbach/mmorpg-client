using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public class GameObjectSettings : MonoBehaviour
{
	#region Private Member Variables
	private Guid _gameObjectID; // ID with in our system.
	private GUIStats _guiStats = null;
    private AudioSource _audioSource;
  //  private Sounds _sounds;
    private AnimationController _animationController;
    private BattleMenu _battleMenu;

	private Vector2 _direction; // Current movement direction of object
	private bool _ally = false;
    private bool _frontRow = true;

	private int _health;
	private int _maxHealth;
	private int _energy;
	private int _maxEnergy;
    private string _resourceName;
	private string _characterName;
    private Animator _anim;
	private SpriteRenderer _sprite;

	#endregion

	#region Public Properties

    public Guid GameObjectID
	{
		get
		{
			return _gameObjectID;
		}
	}
	
	public Vector2 Direction
	{
		get
		{
			return _direction;
		}
	}

	public bool Ally
	{
		get
		{
			return _ally;
		}
	}

    public bool FrontRow
    {
        get
        {
            return _frontRow;
        }
    }

    public int Health
	{
		get
		{
			return _health;
		}
	}
	
	public int MaxHealth
	{
		get
		{
			return _maxHealth;
		}
	}
	
	public int Energy
	{
		get
		{
			return _energy;
		}
	}

	public int MaxEnergy
	{
		get
		{
			return _maxEnergy;
		}
	}

	public string ResourceName
	{
		get
		{
			return _resourceName;
		}
	}

	public string CharacterName
	{
		get
		{
			return _characterName;
		}
	}

    public int Index { get; set; }
    
	#endregion
	
	public void SetMaxHealth(int health)
	{
		_maxHealth = health;
	}
	public void SetMaxEnergy(int energy)
	{
		_maxEnergy = energy;
	}

	public GameObjectSettings()
	{
	}

	protected void Start()
	{
		_audioSource = gameObject.GetComponent<AudioSource> ();
	
		GameObject animation = GameObject.Find ("AnimationController");
		_animationController = animation.GetComponent<AnimationController> ();

        _battleMenu = FindObjectOfType<BattleMenu>();

	}

	public void UpdateHealth(int healthChange, Vector3 screenPos)
	{
		// Spawn scrolling combat text.
		_guiStats.UpdateHealth(healthChange, screenPos);
	}

    public void QueueMessage(string message, Vector3 screenPos)
    {
        _guiStats.QueueMessage(message, screenPos);
    }

	public void UpdateDirection(Vector2 direction)
	{
		_direction = direction.normalized;
	}

	public IEnumerator AnimateSpell(GameObjectSettings target, int id)
	{
        yield return new WaitForSeconds(0f);

        if (id == -1) {
            _audioSource.clip = _battleMenu.AttackClip;
            _audioSource.Play();

            // if it's a strike make enemy flip left and right
            if (_anim == null)
            {
                StartCoroutine(AnimateEnemyAttack());
			} else {
                StartCoroutine(AnimateAttack());
			}

            StartCoroutine(AnimateSlash(target));
		} else {
            switch (id) {
                case 1: // Double strike
                    StartCoroutine(AnimateSpell(target, -1));
                    yield return new WaitForSeconds(2f);
                    StartCoroutine(AnimateSpell(target, -1));
                    
                    break;
                default:
			        // Animate the spell based on our animation controller
			        _animationController.SpellAnimation (this, target, id);
                    break;
            }
		}

	}

    private IEnumerator AnimateEnemyAttack()
    {
        int direction = 1;
        for (int index = 0; index < 6; index++)
        {
            _sprite.transform.localScale = new Vector3(direction, 1, 1);
            direction *= -1;
            yield return new WaitForSeconds(.1f);
        }
    }

    private IEnumerator AnimateSlash(GameObjectSettings target)
    {
        GameObject slashAnimation = (GameObject)Instantiate(_battleMenu.SlashEffectPrefab);
        slashAnimation.transform.position = target.transform.position;

        yield return new WaitForSeconds(1.2f);
        Destroy(slashAnimation);
    }

    private IEnumerator AnimateAttack()
    {
        // if it's a strike make player execute weapon animation
        GameObject attackAnimation = (GameObject)Instantiate(_battleMenu.RotateWeaponPrefab);
        attackAnimation.transform.position = this.transform.position;
        yield return new WaitForSeconds(1.2f);
        Destroy(attackAnimation);
    }

	public void Initialize(Guid gameObjectID, bool ally, bool frontRow, int health, int maxHealth, int energy,
       int maxEnergy, string resourceName, string characterName)
	{
		_gameObjectID = gameObjectID;
		_ally = ally;
        _frontRow = frontRow;
		_health = health;
		_energy = energy;
		_maxHealth = maxHealth;
		_maxEnergy = maxEnergy;
        _characterName = characterName;

		_guiStats = Camera.main.GetComponent<GUIStats> ();

		UpdateStats (ally, frontRow, health, maxHealth, energy, maxEnergy, resourceName);
	}

	public void UpdateStats(bool ally, bool frontRow, int health, int maxHealth, int energy, int maxEnergy, string resourceName)
	{
		_ally = ally;
        _frontRow = frontRow;
		_health = health;
		_energy = energy;
		_maxHealth = maxHealth;
		_maxEnergy = maxEnergy;
		
		if (_resourceName != resourceName) {
			_resourceName = resourceName;
			
			if (ally) {
				_anim = gameObject.GetComponent<Animator>();
				
				_anim.runtimeAnimatorController = GameServer.ClientSettings.FindAnimator(resourceName);
			} else {
				_sprite = gameObject.GetComponent<SpriteRenderer> ();
				_sprite.sprite = GameServer.ClientSettings.FindEnemyTexture(resourceName);
			}
		}
	}

    private Color _buffColor = new Color(1f, 1f, 1f, 1f); // Invisible
    public void ParseBuffString(string buffString)
    {
        Color newColor = new Color(1f, 1f, 1f, 1f);  // Default to invisible
        if (buffString != string.Empty)
        {
            string[] buffStrings = buffString.Split('^');
            for (int index = 0; index < buffStrings.Length; index++)
            {
                if (buffStrings[index].IndexOf('~') > 0)
                {
                    string[] buffElement = buffStrings[index].Split('~');

                    newColor = ParseBuffColor(buffElement[0], int.Parse(buffElement[1]));
                }
            }
        }

        if (!_buffColor.Equals(newColor))
        {
            _buffColor = newColor;

            if (_sprite != null)
                _sprite.material.color = _buffColor;
            else if (_anim != null)
            {
                SpriteRenderer renderer = (SpriteRenderer)_anim.GetComponent<Renderer>();
                renderer.material.color = _buffColor;
            }
        }
    }

    private Color ParseBuffColor(string buffType, int buffAmount)
    {
        // TODO: select color based on various modifiers
        if (buffType == "D")
        {
            return new Color(.5f, .1f, .3f, 1f);
        }
        if (buffType == "S")
        {
            return new Color(.5f, .1f, .3f, 1f);
        }
        if (buffType == "A")
        {
            return new Color(.5f, .1f, .3f, 1f);
        }
        if (buffType == "W")
        {
            return new Color(.5f, .1f, .3f, 1f);
        }
        else
        {
            return new Color(.5f, .1f, .3f, 1f);
        }
    }

    public IEnumerator FaceLeft()
	{
		if (_anim != null) {
			_anim.SetTrigger("WalkLeft");
            yield return new WaitForSeconds(.1f);
			_anim.speed = 0;
		}
	}
	

}
