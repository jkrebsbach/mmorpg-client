﻿using UnityEngine;
using System.Collections;
using System;

public class ProgressBar : MonoBehaviour {

	private double barDisplay = 0.0;
	public Vector2 pos = new Vector2(20,40);
	public Vector2 size = new Vector2(60,20);
	public Texture2D progressBarEmpty;
	public Texture2D progressBarFull;

    private DateTime _finishTime;
    private DateTime _startTime;
	private double _timerMultiplier;
    private ClientSettings _clientSettings;

	void OnGUI()
	{
		if (barDisplay < 1 && barDisplay > 0) {
			// draw the background:
			GUI.BeginGroup (new Rect (pos.x, pos.y, size.x, size.y));
			//GUI.Box (new Rect (0, 0, size.x, size.y), progressBarEmpty);
            GUI.DrawTexture(new Rect(0, 0, size.x, size.y), progressBarEmpty, ScaleMode.StretchToFill);

			
            Rect barSize = new Rect(0, 0, size.x * (float)barDisplay, size.y);

            // This code will stretch the progress bar to fill as needed
            GUI.DrawTexture(barSize, progressBarFull, ScaleMode.StretchToFill);
            
            // This code would repeat progress bar, to make a different visual effect
            //progressBarFull.wrapMode = TextureWrapMode.Repeat;
            //GUI.DrawTextureWithTexCoords(barSize, progressBarFull, new Rect(0, 0, barSize.width / progressBarFull.width, barSize.height / progressBarFull.height));

			GUI.EndGroup ();
		}
	} 
	
	void Update()
	{
		// for this example, the bar display is linked to the current time,
		// however you would set this value based on your desired display
		// eg, the loading progress, the player's health, or whatever.
        barDisplay = (_clientSettings.ServerTime - _startTime).TotalMilliseconds * _timerMultiplier;
	}

    public void InitializeClientSettings(ClientSettings clientSettings)
    {
        _clientSettings = clientSettings;
    }

	public void ResetBar(DateTime finishTime) {
        if (_finishTime != finishTime)
        {
            _finishTime = finishTime;
            _startTime = _clientSettings.ServerTime;

            _timerMultiplier = (1.0 / finishTime.Subtract(_startTime).TotalMilliseconds);
        }
	}
}
