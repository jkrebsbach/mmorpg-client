using System;
using System.Collections.Generic;

public class InventoryItem : Ability
{
	public enum ItemTypeEnum
	{
		Consumable = 1,
        Hand = 2,
        TwoHand = 3,
        Head = 4,
		Body = 5,
        Durable = 6,
        Ranged = 7
	};

    public int Price;
	public int Quantity;
	public ItemTypeEnum ItemType;

	public InventoryItem ()
		: base ()
	{
	}

	public static InventoryItem GetInventoryItem(int abilityID, int quantity)
	{
		InventoryItem result = Ability.GetItem(abilityID);

		result.Quantity = quantity;

		return result;
	}
}
