﻿using UnityEngine;
using System.Collections;

public class FullscreenImage : MonoBehaviour {

    DeviceOrientation orientation;
    SpriteRenderer _sr = null;
		
	// Use this for initialization
	void Start () {
        orientation = Input.deviceOrientation;

        _sr = GetComponent<SpriteRenderer>();
		if (_sr == null) return;

        float width = _sr.sprite.bounds.size.x;
		float height = _sr.sprite.bounds.size.y;
		
		float worldScreenHeight = Camera.main.orthographicSize * 2.0f;
		float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;
		
		transform.localScale = new Vector3(worldScreenWidth / width,worldScreenHeight / height,1);
	}
	
	// Update is called once per frame
	void Update () {
        // Check for screen rotation - update background when this happens
        if (orientation != Input.deviceOrientation)
        {
            if (_sr == null) return;
            
            float width = _sr.sprite.bounds.size.x;
            float height = _sr.sprite.bounds.size.y;

            float worldScreenHeight = Camera.main.orthographicSize * 2.0f;
            float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;

            transform.localScale = new Vector3(worldScreenWidth / width, worldScreenHeight / height, 1);

            orientation = Input.deviceOrientation;
        }
	}
}
