using System;
using System.Collections.Generic;

public class Ability
{
	public int AbilityID;
    public List<int> CharacterTypeID;
	public int[] Cost;
    public int Level;
    public int MaxLevel;
    public decimal[] ShopCostDecrease;
    public string Name;
    public string Description;
	public AbilityType Type;
	public int Cooldown;
    public bool Active;

	public enum AbilityType
	{
		Attack,
		Spell,
        Passive,
        Ranged,
		Item
	};

	protected Ability() {
		}

	public static Ability GetAbility(int abilityID, int level) {
        Ability result = _abilityLibrary[abilityID];
        result.Level = level;

		return result;
	}

    public string FullName(int level)
    {
        if (level > MaxLevel)
            return string.Format("{0} MAX", Name, level);
        else
            return string.Format("{0} Lv {1}", Name, level);
    }

    public static List<Ability> GetCharacterTypeAbilityLibrary(int characterTypeID)
    {
        List<Ability> result = new List<Ability>();

        foreach(Ability ability in _abilityLibrary.Values)
        {
            if (ability.CharacterTypeID.Contains(characterTypeID))
                result.Add(ability);
        }

        return result;
    }

    public static InventoryItem GetItem(int itemID)
    {
        return _itemLibrary[itemID];
    }

	private static Dictionary<int, Ability> _abilityLibrary = new Dictionary<int, Ability> ()
	{
		{ 1, new Ability() {AbilityID = 1, CharacterTypeID= new List<int>() { 1 }, Cost = new int[] {5,7,10}, Name = "Double Strike", MaxLevel=3, Description = "Swing twice - extra damage",
            Type = AbilityType.Spell, Cooldown=10, Active=true }},
		{ 2, new Ability() {AbilityID = 2, CharacterTypeID=new List<int>() { 1 }, Cost = new int[] {4,6,8}, Name = "Mark Target", MaxLevel=3,Description = "Target loses agility",
            Type = AbilityType.Spell, Cooldown=15, Active=true }},
		{ 3, new Ability() {AbilityID = 3, CharacterTypeID=new List<int>() { 1 }, Cost = new int[] {4,6,8}, Name = "Berserk", MaxLevel=3,Description = "Target gains strength",
            Type = AbilityType.Spell, Cooldown=15, Active=true }},
		{ 4, new Ability() {AbilityID = 4, CharacterTypeID=new List<int>() { 2 }, Cost = new int[] {4,6,8}, Name = "Shield", MaxLevel=3,Description = "Target gains defense",
            Type = AbilityType.Spell, Cooldown=15, Active=true }},
        { 5, new Ability() {AbilityID = 5, CharacterTypeID=new List<int>() { 2 }, Cost = new int[] {5,7,10}, Name = "Greek Fire", MaxLevel=3,Description = "Fire damage on all enemies",
            Type = AbilityType.Attack, Cooldown=15, Active=true }},
		{ 6, new Ability() {AbilityID = 6, CharacterTypeID=new List<int>() { 2 }, Cost = new int[] {5,7,10}, Name = "Distract", MaxLevel=3,Description = "Target stoped temporarily",
            Type = AbilityType.Spell, Cooldown=15, Active=true }},
		{ 7, new Ability() {AbilityID = 7, CharacterTypeID=new List<int>() { 2 }, Cost = new int[] {5,7,10}, Name = "Medicine", MaxLevel=3,Description = "Target regains health",
            Type = AbilityType.Passive, Cooldown=12, Active=true }},
		{ 8, new Ability() {AbilityID = 8, CharacterTypeID=new List<int>() { 1 }, Cost = new int[] {0}, Name = "Rugged Defense", MaxLevel=3,Description = "Character permanently gains higher defense",
            Type = AbilityType.Passive, Cooldown=10, Active=false }},
        { 9, new Ability() {AbilityID = 9, CharacterTypeID=new List<int>() { 1 }, Cost = new int[] {0}, Name = "Ruggged Offense", MaxLevel=3,Description = "Character permanently gains higher offense",
            Type = AbilityType.Passive, Cooldown=8, Active=false }},
		{ 10, new Ability() {AbilityID = 10, CharacterTypeID=new List<int>() { 1 }, Cost = new int[] {0}, Name = "Expert Skinner", MaxLevel=4,Description = "Improved enemy drop rate",
            Type = AbilityType.Passive, Cooldown=12, Active=false }},
		{ 11, new Ability() {AbilityID = 11, CharacterTypeID=new List<int>() { 1 }, Cost = new int[] {2,4,6}, Name = "Bow & Arrow", MaxLevel=3,Description = "Ranged attack - Must equip a bow to use",
            Type = AbilityType.Ranged, Cooldown=10, Active=false }},
        { 12, new Ability() {AbilityID = 12, CharacterTypeID=new List<int>() { 2 }, Cost = new int[] {0}, Name = "Clever Salesman", MaxLevel=3,Description = "Improved charisma, improved buy & sell stats",
            Type = AbilityType.Passive, Cooldown=10, Active=false, ShopCostDecrease = new decimal[] {.10M,.20M,.30M}}},
        { 13, new Ability() {AbilityID = 13, CharacterTypeID=new List<int>() { 2 }, Cost = new int[] {0}, Name = "Careful Connosseur", MaxLevel=3,Description = "Chance for item reuse in combat, and chance for reduced ability cost",
            Type = AbilityType.Passive, Cooldown=10, Active=false }},
        { 14, new Ability() {AbilityID = 14, CharacterTypeID=new List<int>() { 2 }, Cost = new int[] {2,4,6}, Name = "Gun", MaxLevel=3,Description = "Ranged attack - Must equip a gun to use",
            Type = AbilityType.Ranged, Cooldown=10, Active=false }}
	};

    private static Dictionary<int, InventoryItem> _itemLibrary = new Dictionary<int, InventoryItem>()
	{
		{ 1, new InventoryItem() {AbilityID = 1, CharacterTypeID= new List<int>() { 1,2 }, Price = 40, Name = "Potion", Description = "Drink me", 
            Type = AbilityType.Item, ItemType = InventoryItem.ItemTypeEnum.Consumable }},
		{ 2, new InventoryItem() {AbilityID = 2, CharacterTypeID= new List<int>() { 1,2 }, Price = 50, Name = "Knife", Description = "Short, but to the point.", 
            Type = AbilityType.Item, ItemType = InventoryItem.ItemTypeEnum.Hand }},
		{ 3, new InventoryItem() {AbilityID = 3, CharacterTypeID= new List<int>() { 1,2 }, Price = 200, Name = "Staff", Description = "A big stick", 
            Type = AbilityType.Item, ItemType = InventoryItem.ItemTypeEnum.Hand }},
		{ 4, new InventoryItem() {AbilityID = 4, CharacterTypeID= new List<int>() { 1 }, Price = 200, Name = "Shield", Description = "Useful for sledding (Woodsman Only)", 
            Type = AbilityType.Item, ItemType = InventoryItem.ItemTypeEnum.Hand }},
		{ 5, new InventoryItem() {AbilityID = 5, CharacterTypeID= new List<int>() { 1 }, Price = 300, Name = "Sword (Two Hands)", Description = "Pointy. (Woodsman Only)", 
            Type = AbilityType.Item, ItemType = InventoryItem.ItemTypeEnum.TwoHand }},
		{ 6, new InventoryItem() {AbilityID = 6, CharacterTypeID= new List<int>() { 1,2 }, Price = 200, Name = "Helmet", Description = "Put it on your head", 
            Type = AbilityType.Item, ItemType = InventoryItem.ItemTypeEnum.Head }},
		{ 7, new InventoryItem() {AbilityID = 7, CharacterTypeID= new List<int>() { 1,2 }, Price = 200, Name = "Armor Light", Description = "Shiny", 
            Type = AbilityType.Item, ItemType = InventoryItem.ItemTypeEnum.Body	}},
        { 8, new InventoryItem() {AbilityID = 8, CharacterTypeID= new List<int>() { 1 }, Price = 300, Name = "Armor Medium", Description = "Shiny (Woodsman Only)", 
            Type = AbilityType.Item, ItemType = InventoryItem.ItemTypeEnum.Body	}},
        { 9, new InventoryItem() {AbilityID = 9, CharacterTypeID= new List<int>() { 2 }, Price = 400, Name = "Armor Special", Description = "Shiny (Merchant Only)", 
            Type = AbilityType.Item, ItemType = InventoryItem.ItemTypeEnum.Body	}},
        { 10, new InventoryItem() {AbilityID = 10, CharacterTypeID= new List<int>() {  }, Price = 10, Name = "Tusk", Description = "Pig drop", 
            Type = AbilityType.Item, ItemType = InventoryItem.ItemTypeEnum.Durable	}},
        { 11, new InventoryItem() {AbilityID = 11, CharacterTypeID= new List<int>() {  }, Price = 10, Name = "Acorn", Description = "Squirrel drop", 
            Type = AbilityType.Item, ItemType = InventoryItem.ItemTypeEnum.Durable	}},
        { 12, new InventoryItem() {AbilityID = 12, CharacterTypeID= new List<int>() { }, Price = 20, Name = "Web", Description = "Sticky", 
            Type = AbilityType.Item, ItemType = InventoryItem.ItemTypeEnum.Durable	}},
        { 13, new InventoryItem() {AbilityID = 13, CharacterTypeID= new List<int>() { }, Price = 20, Name = "Scales", Description = "Slimy", 
            Type = AbilityType.Item, ItemType = InventoryItem.ItemTypeEnum.Durable }},
        { 14, new InventoryItem() {AbilityID = 14, CharacterTypeID= new List<int>() { }, Price = 20, Name = "Stinger", Description = "Also poisonous", 
            Type = AbilityType.Item, ItemType = InventoryItem.ItemTypeEnum.Durable	}},
        { 15, new InventoryItem() {AbilityID = 15, CharacterTypeID= new List<int>() { 1 }, Price = 50, Name = "Bow", Description = "Ranged Weapon (Woodsman Only)", 
            Type = AbilityType.Item, ItemType = InventoryItem.ItemTypeEnum.Ranged	}},
        { 16, new InventoryItem() {AbilityID = 16, CharacterTypeID= new List<int>() { 2 }, Price = 50, Name = "Gun", Description = "Ranged Weapon (Merchant Only)", 
            Type = AbilityType.Item, ItemType = InventoryItem.ItemTypeEnum.Ranged	}},
        { 17, new InventoryItem() {AbilityID = 17, CharacterTypeID= new List<int>() { }, Price = 50, Name = "Dragon Wing", Description = "Dragon drop", 
            Type = AbilityType.Item, ItemType = InventoryItem.ItemTypeEnum.Durable	}},
        { 18, new InventoryItem() {AbilityID = 18, CharacterTypeID= new List<int>() { }, Price = 50, Name = "Goblin Shield", Description = "Too small to use", 
            Type = AbilityType.Item, ItemType = InventoryItem.ItemTypeEnum.Durable	}},
        { 19, new InventoryItem() {AbilityID = 19, CharacterTypeID= new List<int>() { 1,2 }, Price = 50, Name = "Explosive Rocks", Description = "Fire damage - one enemy", 
            Type = AbilityType.Item, ItemType = InventoryItem.ItemTypeEnum.Consumable	}},
        { 20, new InventoryItem() {AbilityID = 20, CharacterTypeID= new List<int>() { 1,2 }, Price = 50, Name = "Slow Powder", Description = "Slow all enemies", 
            Type = AbilityType.Item, ItemType = InventoryItem.ItemTypeEnum.Consumable	}},
        { 21, new InventoryItem() {AbilityID = 21, CharacterTypeID= new List<int>() { 1,2 }, Price = 50, Name = "Ice Crystals", Description = "Ice damage - all enemies", 
            Type = AbilityType.Item, ItemType = InventoryItem.ItemTypeEnum.Consumable	}},
        { 22, new InventoryItem() {AbilityID = 22, CharacterTypeID= new List<int>() { }, Price = 50, Name = "Strange Artifact", Description = "Very unusual", 
            Type = AbilityType.Item, ItemType = InventoryItem.ItemTypeEnum.Durable	}}
	};
}
