﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


// See more at: http://www.theappguruz.com/tutorial/unity-rotate-object-90-degree/#sthash.BSjVXZ80.dpuf
public class WeaponRotation : MonoBehaviour
{
    public int RotationDirection = 1; // -1 for clockwise
    //  1 for anti-clockwise

    public int TargetRotationAngle = 150;
    public int RotationStep = 10;    // should be less than target
    public int RotationIterations;

    // All the objects with which collision will be checked
    public GameObject[] objectsArray;

    private Vector3 initialRotation, currentRotation, targetRotation;
    private int rotationCounter = 1;

    // Use this for initialization
    void Start()
    {
        initialRotation = gameObject.transform.eulerAngles;
        rotateObject();
    }

    private void rotateObject()
    {
        currentRotation = gameObject.transform.eulerAngles;
        targetRotation.z = (currentRotation.z + (TargetRotationAngle * RotationDirection));
        StartCoroutine(objectRotationAnimation());
    }

    IEnumerator objectRotationAnimation()
    {
        // add rotation step to current rotation.
        currentRotation.z += (RotationStep * RotationDirection);
        gameObject.transform.eulerAngles = currentRotation;

        yield return new WaitForSeconds(0);

        if (((int)currentRotation.z > (int)targetRotation.z && RotationDirection < 0) ||  // for clockwise
            ((int)currentRotation.z < (int)targetRotation.z && RotationDirection > 0)) // for anti-clockwise
        {
            StartCoroutine(objectRotationAnimation());
        }
        else
        {
            gameObject.transform.eulerAngles = targetRotation;
            if (rotationCounter < RotationIterations)
            {
                rotationCounter++;
                currentRotation = initialRotation;
                yield return new WaitForSeconds(.2f);
                StartCoroutine(objectRotationAnimation());
            }
        }
    }

    IEnumerator rotateObjectAgain()
    {
        yield return new WaitForSeconds(0.3f);
        rotateObject();
    }

}
