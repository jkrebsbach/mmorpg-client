using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Security.Cryptography;
using System.Text;
using System.IO;

using UnityEngine;
//using Lidgren.Network;

public enum KeyState
{
	// Movement									1xxx
	MoveForward 		= 1001,
	MoveBackwards 		= 1002,
	MoveLeft			= 1003,
	MoveRight			= 1004,
	
	// Ability use								2xxx
	UseSpell 			= 2001,
	
	// Misc use									9xxx
	InteractWithObject 	= 9001
}

public static class GameServer
{
	
	public enum NetworkPackageFromClient
	{
		// Connection Packages 					1xxx
		MOTD 				= 1001,
		WhoAmI				= 1002,
		WhoIsPlayer			= 1003,
		ClientChat			= 1004,
        LearnSkill          = 1005,
        Logout              = 1006,

		// BattleHandler update package 		2xxx
		AttackInitiate		= 2001,
		SpellInitiate 		= 2002,
		InventoryInitiate 	= 2003,
		AttemptDefend		= 2004,
		AttemptEscape		= 2005,
		LeaveBattle			= 2006,
        SwitchRow           = 2007,

		// Overworld activity 					3xxx
		LoadLocations		= 3001,
		CharacterMovement	= 3002,
		UpdateEquipment		= 3003,
		EnterBattle			= 3004,
		EnterLocation		= 3005,
        Respawn             = 3006,

		// Location activity
		LeaveLocation		= 4001,
        StoreInventory      = 4002,
        QuestSelection      = 4003,
        StartQuest          = 4004,
        PurchaseItem        = 4005,
        CompleteQuest       = 4006,
        StartDialog         = 4007,
        SellItem            = 4008
	}
	public enum NetworkPackageToClient
	{
		// Connection Packages 					1xxx
		MOTD 				= 1001,
        WhoAmI 				= 1002,
		WhoIsPlayer			= 1003,
		ChatMessage			= 1004,
		ShutDown			= 1005,
        SkillLearned        = 1006,
        
		// GameObject("any") update package 	2xxx
		UpdateBattleHandler = 2001,
		DestroyBattleHandler = 2002,
		UpdateBattleEntity 	= 2003,
		DestroyBattleEntity	= 2004,
		InitiateGameObject 	= 2005,
		DestroyGameObject	= 2006,
		UpdateHealth 		= 2007,
		UpdateCoolDown		= 2008,
		UpdateSpells		= 2009,
		UpdateInventory		= 2010,
		EscapeAttempt		= 2011,
		InitiateAbility		= 2012,
        BattleReward        = 2013,

		// Overworld Activity
		UpdateLocationHandler = 3001,
		CharacterMovement	= 3002,
		NPCMovement = 3003,
        Respawn = 3004,

		// Location activity
		StoreInventory = 4001,
        QuestSelection = 4002,
		DialogSelection = 4003
	}
	

	// Server Settings
	public static ClientSettings ClientSettings;
	public static Dictionary<int, NPCData> NPCSettings;
    public static Dictionary<int, Dictionary<int, DialogData[]>> QuestDialog;

	static GameServer()
	{
        ParseNPCSettings();
        ParseQuestDialog();
    }

    private static void ParseNPCSettings()
    {
		// Settings for our gameserver when we create it.
		TextAsset dialogJSON = Resources.Load ("Dialog") as TextAsset;
        JSONObject json = new JSONObject(dialogJSON.text);
		NPCSettings = new Dictionary<int, NPCData> ();

		foreach (JSONObject npc in json.list) {
			int npcID = (int)npc["NPC"].n;
            string strResource = npc["Resource"].str;

            DialogData[] dialogData = new DialogData[npc["Dialog"].list.Count];
            int index = 0;

            foreach(JSONObject dialog in npc["Dialog"].list)
			{
                switch(dialog["Type"].str)
                {
                    case "Question":
                        dialogData[index] = new DialogData()
                        {
                            Type = DialogData.DialogType.Question,
                            Text = dialog["Text"].str,
                            Answers = new DialogAnswer[dialog["Answers"].list.Count]
                        };

                        int answerIndex = 0;
                        foreach(JSONObject answer in dialog["Answers"].list)
                        {
                            dialogData[index].Answers[answerIndex] = new DialogAnswer()
                            {
                                Answer = answer["Answer"].str,
                                Next = (int)answer["Next"].f
                            };

                            answerIndex++;
                        }
                        break;
                    case "Text":
                        dialogData[index] = new DialogData()
                        {
                            Type = DialogData.DialogType.Text,
                            Text = dialog["Text"].str,
                            Next = (int)dialog["Next"].f
                        };
                        break;
                    case "Store":
                        dialogData[index] = new DialogData()
                        {
                            Type = DialogData.DialogType.Store,
                            StoreID = (int)dialog["StoreID"].f
                        };
                        break;
                    case "Quest":
                        dialogData[index] = new DialogData()
                        {
                            Type = DialogData.DialogType.Quest,
                            Text = dialog["Text"].str,
                            QuestID = (int)dialog["QuestID"].f
                        };
                        break;
                }

                index++;
            }
			NPCData data = new NPCData(){
				NPCID = npcID,
                Dialog = dialogData,
				Resource = strResource
			};
			NPCSettings[npcID] = data;
		}
	}
	
    private static void ParseQuestDialog()
    {
        // Settings for our gameserver when we create it.
        TextAsset dialogJSON = Resources.Load("QuestDialog") as TextAsset;
        JSONObject json = new JSONObject(dialogJSON.text);
        QuestDialog = new Dictionary<int, Dictionary<int, DialogData[]>>();

        foreach (JSONObject npc in json.list)
        {
            int npcID = (int)npc["NPC"].n;
            int questID = (int)npc["QuestID"].n;

            if (!QuestDialog.ContainsKey(npcID))
                QuestDialog[npcID] = new Dictionary<int, DialogData[]>();
            
            DialogData[] dialogData = new DialogData[npc["Dialog"].list.Count];
            int index = 0;

            foreach (JSONObject dialog in npc["Dialog"].list)
            {
                switch (dialog["Type"].str)
                {
                    case "Question":
                        dialogData[index] = new DialogData()
                        {
                            Type = DialogData.DialogType.Question,
                            Text = dialog["Text"].str,
                            Answers = new DialogAnswer[dialog["Answers"].list.Count]
                        };

                        int answerIndex = 0;
                        foreach (JSONObject answer in dialog["Answers"].list)
                        {
                            dialogData[index].Answers[answerIndex] = new DialogAnswer()
                            {
                                Answer = answer["Answer"].str,
                                Next = (int)answer["Next"].f
                            };

                            answerIndex++;
                        }
                        break;
                    case "Text":
                        dialogData[index] = new DialogData()
                        {
                            Type = DialogData.DialogType.Text,
                            Text = dialog["Text"].str,
                            Next = (int)dialog["Next"].f
                        };
                        break;
                    case "Store":
                        dialogData[index] = new DialogData()
                        {
                            Type = DialogData.DialogType.Store,
                            StoreID = (int)dialog["StoreID"].f
                        };
                        break;
                    case "Quest":
                        dialogData[index] = new DialogData()
                        {
                            Type = DialogData.DialogType.Quest,
                            Text = dialog["Text"].str,
                            QuestID = (int)dialog["QuestID"].f
                        };
                        break;
                }

                index++;
            }

            QuestDialog[npcID][questID] = dialogData;
        }
    }

	public static void Disconnect()
	{
		ClientSettings.Disconnect("");
	}

	#region Updates for the server
	#region InitiateSpell(int spellID)
	public static void AttackInitiate(Guid target)
	{
		NetOutgoingMessage om = new NetOutgoingMessage();
		om.Write(((long)NetworkPackageFromClient.AttackInitiate).ToString());
		om.Write(target.ToString());
		ClientSettings.SendMessage(om);
	}

	public static void WhoAmI()
	{
        if (ClientSettings == null)
            return;

		NetOutgoingMessage om = new NetOutgoingMessage ();
		om.Write (((long)GameServer.NetworkPackageFromClient.WhoAmI).ToString ());
		ClientSettings.SendMessage (om);
	}

	public static void WhoIsPlayer(Guid player)
	{
		NetOutgoingMessage om = new NetOutgoingMessage();
		om.Write(((long)NetworkPackageFromClient.WhoIsPlayer).ToString());
		om.Write(player.ToString());
		ClientSettings.SendMessage(om);
	}

    public static void SendLogout()
    {
        NetOutgoingMessage om = new NetOutgoingMessage();
        om.Write(((long)NetworkPackageFromClient.Logout).ToString());
        ClientSettings.SendMessage(om);
    }

	public static void SendChat(string message)
	{
		NetOutgoingMessage om = new NetOutgoingMessage();
		om.Write(((long)NetworkPackageFromClient.ClientChat).ToString());
		om.Write(message);
		ClientSettings.SendMessage(om);
	}

	public static void SpellInitiate(int ability, Guid target)
	{
		NetOutgoingMessage om = new NetOutgoingMessage();
		om.Write(((long)NetworkPackageFromClient.SpellInitiate).ToString());
		om.Write(ability.ToString());
		om.Write(target.ToString());
		ClientSettings.SendMessage(om);
	}

	public static void InventoryInitiate(int inventoryItem, Guid target)
	{
		NetOutgoingMessage om = new NetOutgoingMessage();
		om.Write(((long)NetworkPackageFromClient.InventoryInitiate).ToString());
		om.Write(inventoryItem.ToString());
		om.Write(target.ToString());
		ClientSettings.SendMessage(om);
	}
	
	public static void EnterBattle(Guid battleID)
	{
		NetOutgoingMessage om = new NetOutgoingMessage();
		om.Write(((long)NetworkPackageFromClient.EnterBattle).ToString());
		om.Write(battleID.ToString());
		ClientSettings.SendMessage(om);
	}

	public static void EnterLocation()
	{
		NetOutgoingMessage om = new NetOutgoingMessage();
		om.Write(((long)NetworkPackageFromClient.EnterLocation).ToString());
		om.Write(ClientSettings.CurrentLocation.ToString());
		ClientSettings.SendMessage(om);
	}

	public static void Respawn()
    {
        NetOutgoingMessage om = new NetOutgoingMessage();
        om.Write(((long)NetworkPackageFromClient.Respawn).ToString());
        ClientSettings.SendMessage(om);
    }

	public static void LeaveBattle(Guid battleID)
	{
		NetOutgoingMessage om = new NetOutgoingMessage();
		om.Write(((long)NetworkPackageFromClient.LeaveBattle).ToString());
		om.Write(battleID.ToString());
		ClientSettings.SendMessage(om);
	}

	public static void LeaveLocation()
	{
		NetOutgoingMessage om = new NetOutgoingMessage();
		om.Write(((long)NetworkPackageFromClient.LeaveLocation).ToString());
		ClientSettings.SendMessage(om);
	}

    public static void RetrieveDialog(int npcID)
    {
        NetOutgoingMessage om = new NetOutgoingMessage();
        om.Write(((long)NetworkPackageFromClient.StartDialog).ToString());
        om.Write(npcID.ToString());
        ClientSettings.SendMessage(om);
    }

    public static void RetrieveStoreInventory(int storeID, int npcID)
    {
        NetOutgoingMessage om = new NetOutgoingMessage();
        om.Write(((long)NetworkPackageFromClient.StoreInventory).ToString());
        om.Write(storeID.ToString());
        om.Write(npcID.ToString());
        ClientSettings.SendMessage(om);
    }

    public static void RetrieveQuests(int npcID)
    {
        NetOutgoingMessage om = new NetOutgoingMessage();
        om.Write(((long)NetworkPackageFromClient.QuestSelection).ToString());
        om.Write(npcID.ToString());
        ClientSettings.SendMessage(om);
    }

    public static void StartQuest(int npcID, int questID)
    {
        NetOutgoingMessage om = new NetOutgoingMessage();
        om.Write(((long)NetworkPackageFromClient.StartQuest).ToString());
        om.Write(npcID.ToString());
        om.Write(questID.ToString());
        ClientSettings.SendMessage(om);
    }

    public static void CompleteQuest(int npcID, int questID)
    {
        NetOutgoingMessage om = new NetOutgoingMessage();
        om.Write(((long)NetworkPackageFromClient.CompleteQuest).ToString());
        om.Write(npcID.ToString());
        om.Write(questID.ToString());
        ClientSettings.SendMessage(om);
    }

    public static void SellItem(int npcID, int storeID, int itemID)
    {
        NetOutgoingMessage om = new NetOutgoingMessage();
        om.Write(((long)NetworkPackageFromClient.SellItem).ToString());
        om.Write(npcID.ToString());
        om.Write(storeID.ToString());
        om.Write(itemID.ToString());
        ClientSettings.SendMessage(om);
    }

    public static void PurchaseItem(int npcID, int storeID, int itemID)
    {
        NetOutgoingMessage om = new NetOutgoingMessage();
        om.Write(((long)NetworkPackageFromClient.PurchaseItem).ToString());
        om.Write(npcID.ToString());
        om.Write(storeID.ToString());
        om.Write(itemID.ToString());
        ClientSettings.SendMessage(om);
    }

    public static void AttemptDefend(Guid battleID)
	{
		NetOutgoingMessage om = new NetOutgoingMessage();
		om.Write(((long)NetworkPackageFromClient.AttemptDefend).ToString());
		ClientSettings.SendMessage(om);
	}
	
	public static void AttemptEscape(Guid battleID)
	{
		NetOutgoingMessage om = new NetOutgoingMessage();
		om.Write(((long)NetworkPackageFromClient.AttemptEscape).ToString());
		ClientSettings.SendMessage(om);
	}

    public static void SwitchRow(Guid battleID)
    {
        NetOutgoingMessage om = new NetOutgoingMessage();
        om.Write(((long)NetworkPackageFromClient.SwitchRow).ToString());
        ClientSettings.SendMessage(om);
    }

    public static void UpdateLocation(Vector3 location, int currentScene)
	{
		NetOutgoingMessage om = new NetOutgoingMessage();
		om.Write(((long)NetworkPackageFromClient.CharacterMovement).ToString());
		om.Write(location.x.ToString());
		om.Write(location.y.ToString());
		om.Write (currentScene.ToString ());
		ClientSettings.SendMessage(om);
	}

	public static void UpdateEquipment()
	{
		NetOutgoingMessage om = new NetOutgoingMessage();
		om.Write(((long)NetworkPackageFromClient.UpdateEquipment).ToString());
		om.Write(ClientSettings.EquipLeftHand.ToString());
		om.Write(ClientSettings.EquipRightHand.ToString());
		om.Write(ClientSettings.EquipHead.ToString());
		om.Write(ClientSettings.EquipBody.ToString());
        om.Write(ClientSettings.EquipRanged.ToString());
		ClientSettings.SendMessage(om);
	}

    public static void LearnSkill(int abilityID)
    {
        NetOutgoingMessage om = new NetOutgoingMessage();
        om.Write(((long)NetworkPackageFromClient.LearnSkill).ToString());
        om.Write(abilityID.ToString());
        ClientSettings.SendMessage(om);
    }

    #endregion


	#endregion
}
