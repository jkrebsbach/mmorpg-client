﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Network
{
    public class MMORPGMessaging : ProcessMessageBase
    {
        public enum NetworkPackageFromClient
        {
            // Connection Packages 					1xxx
            MOTD = 1001,
            WhoAmI = 1002,
            WhoIsPlayer = 1003,
            ClientChat = 1004,
            LearnSkill = 1005,
            Logout = 1006,

            // BattleHandler update package 		2xxx
            AttackInitiate = 2001,
            SpellInitiate = 2002,
            InventoryInitiate = 2003,
            AttemptDefend = 2004,
            AttemptEscape = 2005,
            LeaveBattle = 2006,
            SwitchRow = 2007,

            // Overworld activity 					3xxx
            LoadLocations = 3001,
            CharacterMovement = 3002,
            UpdateEquipment = 3003,
            EnterBattle = 3004,
            EnterLocation = 3005,
            Respawn = 3006,

            // Location activity
            LeaveLocation = 4001,
            StoreInventory = 4002,
            QuestSelection = 4003,
            StartQuest = 4004,
            PurchaseItem = 4005,
            CompleteQuest = 4006,
            StartDialog = 4007,
            SellItem = 4008
        }

        public override void ProcessServerNetworkMessage(ServerNetworkMessage message)
        {
            if (Enum.IsDefined(typeof(GameServer.NetworkPackageToClient), message.PackID))
            {
                GameServer.NetworkPackageToClient networkPackage = (GameServer.NetworkPackageToClient)message.PackID;

                switch (networkPackage)
                {
                    case GameServer.NetworkPackageToClient.MOTD:
                        break;
                    case GameServer.NetworkPackageToClient.WhoAmI:
                        ClientSettings.UpdateWhoAmI(message.Pieces);
                        break;
                    case GameServer.NetworkPackageToClient.WhoIsPlayer:
                        if (Application.loadedLevel == 2 || Application.loadedLevel == 3) // Overworld scene or location scene
                            ClientSettings.SetupCharacter(message.Pieces);
                        break;
                    case GameServer.NetworkPackageToClient.ChatMessage:
                        ClientSettings.ChatMessage(message.Pieces);
                        break;
                    case GameServer.NetworkPackageToClient.ShutDown:
                        ClientSettings.Shutdown();
                        break;
                    case GameServer.NetworkPackageToClient.SkillLearned:
                        ClientSettings.SkillLearnedQueue.Push(message.Pieces);
                        break;
                    case GameServer.NetworkPackageToClient.UpdateBattleHandler:
                        if (Application.loadedLevel == 2 || Application.loadedLevel == 3) // Overworld scene or location scene
                            ClientSettings.UpdateBattleHandler(message.Pieces);
                        break;
                    case GameServer.NetworkPackageToClient.Respawn:
                        ClientSettings.Respawn();
                        break;
                    case GameServer.NetworkPackageToClient.UpdateLocationHandler:
                        ClientSettings.SetupLocation(message.Pieces);
                        break;
                    case GameServer.NetworkPackageToClient.DestroyBattleHandler:
                        ClientSettings.DestroyBattleHandler(message.Pieces);
                        break;
                    case GameServer.NetworkPackageToClient.UpdateBattleEntity:
                        if (Application.loadedLevel == 1) // Battle scene
                            ClientSettings.UpdateBattleEntityQueue.Push(message.Pieces);
                        break;
                    case GameServer.NetworkPackageToClient.DestroyBattleEntity:
                        if (Application.loadedLevel == 1) // Battle scene
                            ClientSettings.DeleteBattleEntityQueue.Push(message.Pieces);
                        break;
                    case GameServer.NetworkPackageToClient.UpdateHealth:
                        ClientSettings.HealthQueue.Push(message.Pieces);
                        break;
                    case GameServer.NetworkPackageToClient.UpdateSpells:
                        ClientSettings.SetupSpells(message.Pieces);
                        break;
                    case GameServer.NetworkPackageToClient.UpdateInventory:
                        ClientSettings.SetupInventory(message.Pieces);
                        break;
                    case GameServer.NetworkPackageToClient.EscapeAttempt:
                        if (Application.loadedLevel == 1) // Battle scene
                            ClientSettings.EscapeAttemptQueue.Push(message.Pieces);
                        break;
                    case GameServer.NetworkPackageToClient.BattleReward:
                        if (Application.loadedLevel == 1) // Battle scene
                            ClientSettings.BattleRewardQueue.Push(message.Pieces);
                        break;
                    case GameServer.NetworkPackageToClient.InitiateAbility:
                        if (Application.loadedLevel == 1) // Battle scene
                            ClientSettings.InitiateAbilityQueue.Push(message.Pieces);
                        break;
                    case GameServer.NetworkPackageToClient.CharacterMovement:
                        if (Application.loadedLevel == 2 || Application.loadedLevel == 3) // Overworld scene or Location scene
                            ClientSettings.MoveCharacter(message.Pieces);
                        break;
                    case GameServer.NetworkPackageToClient.NPCMovement:
                        if (Application.loadedLevel == 3) // Location scene
                            ClientSettings.UpdateNPCQueue.Push(message.Pieces);
                        break;
                    case GameServer.NetworkPackageToClient.StoreInventory:
                        ClientSettings.StoreInventoryQueue.Push(message.Pieces);
                        break;
                    case GameServer.NetworkPackageToClient.QuestSelection:
                        ClientSettings.QuestQueue.Push(message.Pieces);
                        break;
                    case GameServer.NetworkPackageToClient.DialogSelection:
                        ClientSettings.DialogQueue.Push(message.Pieces);
                        break;
                }
            }
            else
            {
                Debug.Log(String.Format("Unexpected message ID: {0}", message.PackID));
            }
        }
    }
}
