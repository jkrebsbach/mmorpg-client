using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;
using System.Threading;

#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_WEBPLAYER || UNITY_WEBGL
using System.Net.Sockets;
using System.Net;
#elif UNITY_METRO
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Networking;
using Windows.Networking.Sockets;
using Windows.Storage;
using Windows.Storage.Streams;
#endif



public partial class ClientSettings
{

#region Private Member Variables
	private long _currentTimezoneOffset = 0;

	private List<Ability> _spells = new List<Ability>(); // List of spells owned by character
	private List<InventoryItem> _inventory = new List<InventoryItem>(); // List of items owned by character
	private bool _respawn = false;

    public bool Dead { get { return _respawn; } }

	public List<LocationHandler.LocationSetting> Locations = new List<LocationHandler.LocationSetting>();
	public int MaxHealth;
	public int Health;
	public int MaxEnergy;
	public int Energy;

    public int Gold;
    public int CharacterLevel;
    public int Experience;
	public int EquipLeftHand;
	public int EquipRightHand;
	public int EquipHead;
	public int EquipBody;
    public int EquipRanged;

    public int CharacterTypeID;
    public int SkillPoints;
    public int CurrentScene = -1;
    public int CurrentLocation = -1;
    public float LoginX;
    public float LoginY;
    public long CurrentTimezoneOffset;

#endregion



#region Public Properties
	public float cameraDistance;
	public float cameraAngle;
	public GameObject[] OverworldPrefabs;
	public RuntimeAnimatorController[] CharacterAnimations;
	
	private List<Battle> _battles = new List<Battle>();
	private List<Location> _locations = new List<Location>();
	private Dictionary<Guid, MovingEntity> _players = new Dictionary<Guid, MovingEntity>();
    public Stack<String[]> SkillLearnedQueue = new Stack<String[]>();
	public Stack<String[]> UpdateBattleEntityQueue = new Stack<String[]> ();
	public Stack<String[]> DeleteBattleEntityQueue = new Stack<String[]> ();
	public Stack<String[]> HealthQueue = new Stack<String[]> ();
	public Stack<String[]> EscapeAttemptQueue = new Stack<String[]> ();
    public Stack<String[]> BattleRewardQueue = new Stack<String[]>();
	public Stack<String[]> InitiateAbilityQueue = new Stack<String[]> ();
    public Stack<String[]> StoreInventoryQueue = new Stack<String[]>();
    public Stack<String[]> DialogQueue = new Stack<String[]>();
    public Stack<String[]> QuestQueue = new Stack<String[]>();

	public bool Mute
    {
        get
        {
            return PlayerPrefs.GetInt("Mute") == 1;
        }
        set
        {
            PlayerPrefs.SetInt("Mute", value ? 1 : 0);
        }
    }

    
	public class BattleEvent
	{
		public Guid BattleGuid;
		public Vector3 BattlePosition;
		public Vector3 BattleWander;
		public List<int> AllyTypes;
        public int Scene;
	}

	public List<Ability> Spells
	{
		get
		{
			return _spells;
		}
	}

	public List<InventoryItem> Inventory
	{
		get
		{
			return _inventory;
		}
	}

    public int NextLevel
    {
        get
        {
            switch (CharacterLevel)
            {
                case 0:
                    return 0;
                case 1:
                    return 100;
                case 2:
                    return 200;
                case 3:
                    return 400;
                case 4:
                    return 1000;
                case 5:
                    return 2500;
                case 6:
                    return 6000;
                case 7:
                    return 15000;
                case 8:
                    return 45000;
                case 9:
                    return 100000;
                default:
                    return 999999;
            }
        }
    }

	
#endregion
    
	
    

	public GameObject RetrieveCharacterPrefab (int index)
	{
		GameObject res = (GameObject)Instantiate (OverworldPrefabs [index]);

		return res;
	}
	
	public GameObject RetrieveOverworldPrefab (string resourceName, int index, bool location)
	{
		GameObject res = (GameObject)Instantiate (OverworldPrefabs [index]);
		
        Sprite sprite = null;
        if (location)
        {
            resourceName = resourceName.Substring(resourceName.LastIndexOf('/') + 1);
            sprite = FindOverworldSprite(resourceName);
        }
        else
        {
            string resourcePath = string.Format("Textures/Enemies/{0}", resourceName);
            sprite = Resources.Load<Sprite>(resourcePath);

            if (sprite == null && resourceName.IndexOf('_') > 0)
            {
                var spriteSheet = Resources.LoadAll<Sprite>(resourcePath.Substring(0, resourcePath.LastIndexOf('_')));

                sprite = spriteSheet.FirstOrDefault(ss => ss.name == resourceName);
            }
        }

        SpriteRenderer sr = res.GetComponent<SpriteRenderer> ();
		if (sr != null) {
            sr.sprite = sprite;
		} else {
			GUITexture guiTexture = (GUITexture)res.GetComponent<GUITexture> ();
            guiTexture.texture = sprite.texture;
		}
		
		return res;
	}
	
	public Sprite FindEnemyTexture (string resourceName)
	{
		string resourcePath = string.Format("Textures/Enemies/{0}", resourceName);
        Sprite sprite = Resources.Load<Sprite>(resourcePath);

        if (sprite == null && resourceName.IndexOf('_') > 0)
        {
            var spriteSheet = Resources.LoadAll<Sprite>(resourcePath.Substring(0, resourcePath.LastIndexOf('_')));

            sprite = spriteSheet.FirstOrDefault(ss => ss.name == resourceName);
        }

        return sprite;
	}

	public RuntimeAnimatorController FindAnimator(string resourceName)
	{
		int animatorIndex = 0;
		switch (resourceName) {
		case "woodsman":
			animatorIndex = 1;
			break;
        case "merchant":
			animatorIndex = 2;
			break;
		}

		return CharacterAnimations [animatorIndex];
	}
	
	public Sprite FindOverworldSprite(string resourceName)
	{
        string resourcePath = string.Format("Textures/{0}", resourceName);
        return Resources.Load<Sprite>(resourcePath);
	}
	
	

		public void SetupSpells(string[] msg)
		{
			_spells.Clear ();
			for (int index = 1; index < msg.Length; index++) 
			{
				string[] spell = msg[index].Split ('~');
				int abilityID = 0;
                int level = 0;

				if (int.TryParse(spell[0], out abilityID) && int.TryParse(spell[1], out level)) {
					_spells.Add(Ability.GetAbility(abilityID, level));
				}
			}
		}

    public void SetupInventory(string[] msg)
		{
			_inventory.Clear ();
			for (int index = 1; index < msg.Length; index++) 
			{
				string[] inventory = msg[index].Split ('~');
				int abilityID = 0;
				int quantity = 0;

				if (int.TryParse(inventory[0], out abilityID) && int.TryParse(inventory[1], out quantity)) {
					_inventory.Add(InventoryItem.GetInventoryItem(abilityID, quantity));
				}
			}
		}

    public void UpdateWhoAmI (string[] msg)
	{

		string myID = msg [1];

		if (_whoAmI == Guid.Empty) {
				_whoAmI = new Guid (msg [1]);
		}
		MaxHealth = int.Parse(msg [2]);
		Health = int.Parse (msg [3]);
		MaxEnergy = int.Parse(msg [4]);
		Energy = int.Parse (msg [5]);
        Gold = int.Parse(msg[6]);
        
        EquipLeftHand = int.Parse (msg [7]);
		EquipRightHand = int.Parse (msg [8]);
		EquipBody = int.Parse (msg [9]);
		EquipHead = int.Parse (msg [10]);
        EquipRanged = int.Parse(msg[19]);

        CharacterTypeID = int.Parse(msg[11]);
        SkillPoints = int.Parse(msg[12]);

        CurrentLocation = int.Parse(msg[13]);
        CurrentScene = int.Parse(msg[14]);
        LoginX = float.Parse(msg[15]);
        LoginY = float.Parse(msg[16]);
        CharacterLevel = int.Parse(msg[17]);
        Experience = int.Parse(msg[18]);

		Debug.Log ("We are : " + myID);
	}

    public void ChatMessage(string[] pieces)
    {
        _chatter.ProcessMessage(pieces);
    }

    public void SetupCharacter(String[] message)
	{
		Guid playerID = new Guid (message [1]);
		String textureName = message [2];
		String characterName = message [3];
		
		if (!_players.ContainsKey (playerID)) {
			GameObject playerObject = RetrieveCharacterPrefab(0);
			
			Animator anim = playerObject.GetComponent<Animator>();
			anim.runtimeAnimatorController = FindAnimator(textureName);
			
			_players[playerID] = playerObject.GetComponent<MovingEntity>();
		}
		
		MovingEntity movingPlayer = _players [playerID];
		movingPlayer.SetCharacterName (characterName);
	}

    public void MoveCharacter(String[] message)
	{
		Guid playerID = new Guid (message [1]);
		float positionX = float.Parse (message [2]);
		float positionY = float.Parse (message [3]);
        int scene = int.Parse(message[4]);

        if (CurrentScene != scene)
            return;

		if (_players.ContainsKey (playerID)) {
			MovingEntity movingPlayer = _players [playerID];
			movingPlayer.SetServerPosition (new Vector2 (positionX, positionY));
		} else {
			GameServer.WhoIsPlayer(playerID);
		}
	}

    public void UpdateBattleHandler (string[] msg)
	{
        if (msg.Length < 8)
        {
            Debug.Log("incomplete UpdateBattleHandler message " + msg.Length.ToString());
            return;
        }

		Guid guid = new Guid (msg [1]);
		float positionX = float.Parse (msg [2]);
		float positionY = float.Parse (msg [3]);
		float wanderX = float.Parse (msg [4]);
		float wanderY = float.Parse (msg [5]);
		string strAllyTypes = msg [6];
        int scene = int.Parse(msg[7]);
        string resource = msg[8];

        if (CurrentScene != scene)
            return;

		Vector3 position = new Vector3 (positionX, positionY, 0f);
		Vector3 wander = new Vector3 (wanderX, wanderY, 0f);
        List<int> allyTypes = new List<int>();
        if (strAllyTypes != string.Empty)
        {
            foreach (var at in strAllyTypes.Split('~'))
            {
                allyTypes.Add(int.Parse(at));
            }
        }

		BattleEvent battleEvent = new BattleEvent ()
		{
			BattleGuid = guid,
			BattlePosition = position,
			BattleWander = wander,
            AllyTypes = allyTypes,
            Scene = scene
		};
		
        foreach (Battle battle in _battles) {
            // Safe check that object isn't being disposed by unity
            if (battle != null && battle.BattleID == battleEvent.BattleGuid)
			{
                battle.UpdateBattle(battleEvent);
				return;
			}
		}

        GameObject battleObject = RetrieveOverworldPrefab(resource, 1, false);
		Battle newBattle = battleObject.GetComponent<Battle>();
		
		newBattle.BattleID = battleEvent.BattleGuid;
        newBattle.InitializeBattle(battleEvent);

		_battles.Add (newBattle);
	}

    public void Respawn()
    {
        _respawn = false;
    }

	public void ClearOverworld() {
        foreach(var ba in _battles) {
            if (ba != null && ba.gameObject != null)
            {
                Destroy(ba.gameObject);
            }
        }

        foreach (var lo in _locations)
        {
            if (lo != null && lo.gameObject != null)
                Destroy(lo.gameObject);
        }

        foreach (MovingEntity movingEntity in _players.Values)
        {
            if (movingEntity != null && movingEntity.gameObject != null)
                Destroy(movingEntity.gameObject);
        }

		_battles = new List<Battle>();
		_locations = new List<Location>();
		_players = new Dictionary<Guid, MovingEntity>();
	}

	public void LoadLocations() {
        if (_respawn)
        {
            GameServer.Respawn();
        }

    	foreach (LocationHandler.LocationSetting setting in Locations) {
			GameObject locationObject = RetrieveOverworldPrefab (setting.Resource, 2, true);
			Location newLocation = locationObject.GetComponent<Location>();
			
			newLocation.LocationID = setting.LocationID;
            newLocation.Resource = setting.Resource;
            newLocation.PlayerStart = setting.PlayerStart;
			locationObject.transform.position = setting.OverworldPosition;
			
			_locations.Add(newLocation);
		}
	}

    public void SetupLocation(String[] msg) {
        Locations.Clear();
		for (int index = 1; index < msg.Length; index++) {
			string[] locationAttributes = msg[index].Split ('~');

			if (locationAttributes.Length < 6)
				continue;

			int locationID = Int32.Parse(locationAttributes [0]);
			float positionX = float.Parse (locationAttributes [1]);
			float positionY = float.Parse (locationAttributes [2]);
			string resource = locationAttributes [3];
            float startX = float.Parse(locationAttributes[4]);
            float startY = float.Parse(locationAttributes[5]);
			
			Vector3 position = new Vector3 (positionX, positionY, 0f);
            Vector3 start = new Vector3(startX, startY, 0f);
			
			foreach (LocationHandler.LocationSetting location in Locations) {
                if (location.LocationID == locationID)
                {
					// Update location
					continue;
				}
			}

			LocationHandler.LocationSetting newLocation = new LocationHandler.LocationSetting()
			{
                LocationID = locationID,
                Resource = resource,
				OverworldPosition = position,
                PlayerStart = start
			};

			Locations.Add(newLocation);
		}

	}

    public void Death()
    {
        _respawn = true;
    }

    public void DestroyBattleHandler (string[] msg)
		{
            if (msg.Length < 6)
                return;

            Guid guid = new Guid(msg[1]);

            Battle oldBattle = _battles.FirstOrDefault(b => b.BattleID == guid);
            if (oldBattle != null)
            {
                _battles.Remove(oldBattle);
                oldBattle.MarkDisposed();
            }
		}

		internal DateTime ServerTime
        {
		    get { return DateTime.UtcNow.AddTicks(_currentTimezoneOffset); }
        }

        internal InventoryItem HasExtra(InventoryItem inventoryItem)
        {
            InventoryItem result = null;
            int baseCount = 0;

            if (EquipHead == inventoryItem.AbilityID)
            {
                baseCount++;
            }
            if (EquipBody == inventoryItem.AbilityID)
            {
                baseCount++;
            }
            if (EquipLeftHand == inventoryItem.AbilityID)
            {
                baseCount++;
            }
            if (EquipRightHand == inventoryItem.AbilityID && inventoryItem.ItemType != InventoryItem.ItemTypeEnum.TwoHand)
            {
                baseCount++;
            }
            if (EquipRanged == inventoryItem.AbilityID)
            {
                baseCount++;
            }

            if (inventoryItem.Quantity > baseCount)
            {
                result = new InventoryItem()
                {
                    AbilityID = inventoryItem.AbilityID,
                    Price = inventoryItem.Price,
                    Description = inventoryItem.Description,
                    Name = inventoryItem.Name,
                    Quantity = (inventoryItem.Quantity - baseCount)
                };
            }

            return result;
        }
}