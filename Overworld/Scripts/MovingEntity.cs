using UnityEngine;
using System;
using System.Collections;

public class MovingEntity : MonoBehaviour
{
	private Animator _anim;
    private bool _initialized = false;

	private int walkUpHash = Animator.StringToHash("WalkUp");
	private int walkDownHash = Animator.StringToHash("WalkDown");
	private int walkLeftHash = Animator.StringToHash("WalkLeft");
	private int walkRightHash = Animator.StringToHash("WalkRight");
	
	private Vector3 _serverPosition = Vector3.zero;
	private float _startTime;
	private float _duration = 5f; 
	private String _characterName;

	protected void Start() {
		_anim = GetComponent<Animator> ();
	}

	// Update is called once per frame - We want to slowly slide into the location the server is reporting
	protected void Update () {
		if (_serverPosition != transform.position) {
			_anim.SetTrigger("");
			_anim.speed = 4f;

			Vector3 currentDirection = _serverPosition - transform.position;
			if (currentDirection.x > 0) {
				_anim.SetTrigger (walkRightHash);
			} else if (currentDirection.x < 0) {
				_anim.SetTrigger (walkLeftHash);
			} else if (currentDirection.y > 0) {
				_anim.SetTrigger (walkUpHash);
			} else if (currentDirection.y < 0) {
				_anim.SetTrigger (walkDownHash);
			}

			transform.position = Vector3.Lerp (transform.position, _serverPosition, (Time.time - _startTime) / _duration);
		} else {
			_anim.speed = 0f;
		}

        // Check if we have not received an update in 2 seconds - if so, character has likely left
        if (_initialized && Time.time - _startTime > 2)
        {
            Destroy(gameObject);
        }
	}

    public void SetServerPosition(Vector3 position)
	{
        if (_initialized) {
		    _serverPosition = position;
		    _startTime = Time.time;
        } else {
            transform.position = position;
            _serverPosition = position;
            _startTime = Time.time;
            _initialized = true;
        }
	}

	public void SetCharacterName(String name)
	{
		_characterName = name;
	}
}