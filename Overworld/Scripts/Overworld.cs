﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Overworld : MapHandler {
	public int movementSpeed = 2;
	
    // Use this for initialization
	protected override void Start () {
        base.Start();

        _clientSettings.LoadLocations();

        _player.transform.localScale = new Vector3(0.2f, 0.2f, 0);
	}
	
	// Update is called once per frame
	protected void Update () {

        if (_lastLocationRefresh < _clientSettings.ServerTime && _clientSettings.WhoAmI != Guid.Empty)
        {
			GameServer.UpdateLocation(_player.transform.position, -1);

            _lastLocationRefresh = _clientSettings.ServerTime.AddSeconds(1);
		}

        if (_clientSettings.LoginFailure)
        {
            Debug.Log("Server shutdown unexpectedly");
            Application.LoadLevel("MainScene");
        }

		if (_player != null) {
			// Follow player with camera
			Vector3 playerPosition = _player.transform.position;
			float x = playerPosition.x;
			float y = playerPosition.y;

            /*
             * -- This code allows for endless circling maps
             * 
			bool newPosition = false;
			// Once we step off the map, reset to the other side
			if (playerPosition.x < -20) {
				x = 20f;
				newPosition = true;
			}
			if (playerPosition.x > 20) {
				x = -20f;
				newPosition = true;
			}
			if (playerPosition.y < -10) {
				y = 10f;
				newPosition = true;
			}
			if (playerPosition.y > 10) {
				y = -10f;
				newPosition = true;
			}

			if (newPosition) {
				playerPosition = new Vector3(x, y, playerPosition.z);
				_player.transform.position = playerPosition;
			}
            */

			playerPosition.z = transform.position.z;
			transform.position = playerPosition;

		}
	}




}
