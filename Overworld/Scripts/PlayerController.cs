﻿using UnityEngine;
using System;
using System.Collections;
using Assets.Settings.Scripts;

public class PlayerController : MonoBehaviour {
	ClientSettings _clientSettings = null;
	private Animator anim = null;
	private CharacterController controller = null;
	public float moveSpeed = 0.5f;

	private int walkUpHash = Animator.StringToHash("WalkUp");
	private int walkDownHash = Animator.StringToHash("WalkDown");
	private int walkLeftHash = Animator.StringToHash("WalkLeft");
	private int walkRightHash = Animator.StringToHash("WalkRight");

	private Vector3 currentDirection = Vector3.zero;
	private int currentAnimation = 0;
	private float xAxis = 0f; // These are for the GUI buttons
	private float yAxis = 0f; // These are for the GUI buttons

    private bool _inDialog = false;
    private DateTime _invulnerability = DateTime.MaxValue;

    GUIStyle labelStyle = null;
    GUIStyle buttonStyle = null;
    private bool _skinInitialized = false;
    
    // Use this for initialization
	protected void Start () {
		String resourceName = PlayerPrefs.GetString ("resourceName");
		
		_clientSettings = FindObjectOfType<ClientSettings> ();

		anim = GetComponent<Animator> ();
		anim.runtimeAnimatorController = _clientSettings.FindAnimator (resourceName);

		controller = GetComponent<CharacterController> ();

		currentAnimation = walkDownHash;
		anim.SetTrigger(currentAnimation);
	}

	// Update is called once per frame
	protected void Update () {
		float x = Input.GetAxis("Horizontal");
		float y = Input.GetAxis("Vertical");
		Vector3 keyboardMovementVector = new Vector3(x, y);
		Vector3 mouseMovementVector = new Vector3(xAxis, yAxis);
		Vector3 movementVector = (keyboardMovementVector == Vector3.zero ? mouseMovementVector : keyboardMovementVector);
		
		if (movementVector != Vector3.zero) {
			Vector3 currentPosition = transform.position;
			
			//transform.position = Vector3.Lerp (currentPosition, target, Time.deltaTime);

			controller.Move (transform.TransformDirection(movementVector) * moveSpeed * Time.deltaTime);

			anim.speed = 1;

			if (movementVector != currentDirection) {
				currentDirection = movementVector;
				anim.speed = 1;

				int newAnimation = 0;
				if (currentDirection.x > 0) {
					newAnimation = walkRightHash;
				} else if (currentDirection.x < 0) {
					newAnimation = walkLeftHash;
				} else if (currentDirection.y > 0) {
					newAnimation = walkUpHash;
				} else if (currentDirection.y < 0) {
					newAnimation = walkDownHash;
				}

				// Problems when we double-enter an animation state...  just avoid it...
				if (currentAnimation != newAnimation) {
					anim.SetTrigger (newAnimation);
					currentAnimation = newAnimation;
				}
			}
		} else {
			anim.speed = 0;
		}
	}

	protected void OnGUI() 
	{
        if (!_skinInitialized)
        {
            labelStyle = GUI.skin.label;
            buttonStyle = GUI.skin.button;

            // Custom formatting for small screen sizes
            if (Screen.width < 768)
            {
                labelStyle.fontSize = 15.DpToPixel();
                buttonStyle.fontSize = 15.DpToPixel();
                GUI.skin.verticalScrollbar.fixedWidth = Screen.width * 0.05f;
                GUI.skin.verticalScrollbarThumb.fixedWidth = Screen.width * 0.05f;
            }
            else
            {
                // Default styles
            }

            _skinInitialized = true;
        }
        if (!_inDialog && !_clientSettings.Dead)
        {
            yAxis = 0f;
            xAxis = 0f;
            
            if (GUI.RepeatButton(new Rect(Screen.width * .8f, Screen.height * .6f,
                                            Screen.width * .1f, Screen.height * .1f), "\u25B2", buttonStyle)) // Up
            {
                yAxis = 1f;
            }
            if (GUI.RepeatButton(new Rect(Screen.width * .8f, Screen.height * .8f,
                                            Screen.width * .1f, Screen.height * .1f), "\u25BC", buttonStyle)) // Down
            {
                yAxis = -1f;
            }
            if (GUI.RepeatButton(new Rect(Screen.width * .75f, Screen.height * .70f,
                                            Screen.width * .1f, Screen.height * .1f), "\u25C0", buttonStyle)) // Left
            {
                xAxis = -1f;
            }
            if (GUI.RepeatButton(new Rect(Screen.width * .85f, Screen.height * .70f,
                                            Screen.width * .1f, Screen.height * .1f), "\u25B6", buttonStyle)) // Right
            {
                xAxis = 1f;
            }
        }
	}

	protected void OnCollisionEnter(Collision collision)
	{
	}

	protected void OnTriggerEnter2D(Collider2D collider) {

	}

    protected void OnTriggerExit(Collider collider)
    {
        if (collider.name.ToLower().Contains("npc"))
        {
            NPC npc = collider.GetComponent<NPC>();

            npc.HideAction();
        }
    }

	protected void OnTriggerEnter(Collider collider)
	{
        if (_clientSettings.Dead)
            return;

		if (collider.name.ToLower ().Contains ("battle")) {
            // Give the player a few seconds to look at map before jumping into combat
            if (_invulnerability > _clientSettings.ServerTime)
                return;

			Battle battle = collider.GetComponent<Battle> ();

			_clientSettings.ClearOverworld ();

			PlayerPrefs.SetFloat("PlayerX", battle.transform.position.x);
            PlayerPrefs.SetFloat("PlayerY", battle.transform.position.y);
            PlayerPrefs.SetString("battleID", battle.BattleID.ToString());
			PlayerPrefs.Save ();
			Application.LoadLevel ("BattleScene");
		} else if (collider.name.ToLower ().Contains ("location")) {
			Location location = collider.GetComponent<Location> ();

            _clientSettings.CurrentScene = 0;
            _clientSettings.CurrentLocation = location.LocationID;
            _clientSettings.ClearOverworld();
            
			PlayerPrefs.SetFloat ("PlayerX", location.transform.position.x);
			PlayerPrefs.SetFloat ("PlayerY", location.transform.position.y);
            PlayerPrefs.SetFloat("PlayerSceneX", location.PlayerStart.x);
            PlayerPrefs.SetFloat("PlayerSceneY", location.PlayerStart.y);
			PlayerPrefs.SetString ("locationResource", location.Resource);

			PlayerPrefs.Save ();
			Application.LoadLevel ("LocationScene");
		}  else if (collider.name.ToLower ().Contains ("door")) {
			Door door = collider.GetComponent<Door> ();
            
            if (door.TargetSceneID == -1) {
				// Exit location
                GameServer.LeaveLocation();

                _clientSettings.CurrentScene = -1;
                _clientSettings.ClearOverworld();
                
                // Find current location map coordinated
                foreach (var location in _clientSettings.Locations)
                {
                    if (location.LocationID == _clientSettings.CurrentLocation)
                    {
                        PlayerPrefs.SetFloat("PlayerX", location.OverworldPosition.x);
                        PlayerPrefs.SetFloat("PlayerY", location.OverworldPosition.y - 0.7f);
                    }
                }

                _clientSettings.CurrentLocation = 0;
                Application.LoadLevel("OverworldScene");
			} else {
                this.GetComponent<Collider>().enabled = false;
                LocationHandler locationHandler = FindObjectOfType<LocationHandler>();
				locationHandler.LoadScene (door.TargetSceneID);
				
				Vector3 targetLocation = new Vector3(door.TargetLocation.x, door.TargetLocation.y, 0);
				transform.position = targetLocation;
                this.GetComponent<Collider>().enabled = true;
			}
        }
        else if (collider.name.ToLower().Contains("npc"))
        {
            NPC npc = collider.GetComponent<NPC>();

            npc.ShowAction();
        }

	}

    public void EnterDialog()
    {
        _inDialog = true;
    }

    public void ExitDialog()
    {
        _inDialog = false;
    }

    internal void SetInvulnerable(DateTime dateTime)
    {
        _invulnerability = dateTime;
    }
}
