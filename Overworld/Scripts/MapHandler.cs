﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapHandler : MonoBehaviour
{
    public Texture2D settingsButton;
    protected GameObject _player;
    protected ClientSettings _clientSettings = null;
    protected DateTime _lastLocationRefresh = DateTime.MinValue;

    // Use this for initialization
    protected virtual void Start()
    {
        _clientSettings = FindObjectOfType<ClientSettings>();
        _player = GameObject.Find("player");

        bool alive = !_clientSettings.Dead;

        if (alive && PlayerPrefs.HasKey("PlayerX"))
        {
            Vector3 location = new Vector3(PlayerPrefs.GetFloat("PlayerX"), PlayerPrefs.GetFloat("PlayerY"), 0f);
            _player.transform.position = location;
        }

        PlayerController playerController = _player.GetComponent<PlayerController>();
        // Give the player a few seconds to look at map before entering battles
        playerController.SetInvulnerable(_clientSettings.ServerTime.AddSeconds(1.5));
    }
	
    protected virtual void OnGUI()
    {
        if (_clientSettings.Dead)
        {
            Rect dialogRect = new Rect(Screen.width * .1f, Screen.height * .1f, Screen.width * .8f, Screen.height * .3f);

            GUI.Box(dialogRect, string.Empty);
            GUI.Label(dialogRect, "Respawning - Please wait....");
        }

        if (GUI.Button(new Rect(10, 10, 100, 100), settingsButton))
        {
            PlayerPrefs.SetFloat("PlayerX", _player.transform.position.x);
            PlayerPrefs.SetFloat("PlayerY", _player.transform.position.y);
            PlayerPrefs.Save();
            Application.LoadLevel("SettingsScene");
        }
    }
}