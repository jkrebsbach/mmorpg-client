﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Battle : MonoBehaviour {

	public Guid BattleID;
	public Vector3 Wander;
	private float moveSpeed = 1f;

    private bool _initialized = false;
    private bool _disposed = false;
    private float _startTime;

    private ClientSettings _clientSettings = null;
    private List<int> _allyTypes;
	
	// Use this for initialization
	void Start () {
        _clientSettings = FindObjectOfType<ClientSettings>();
 	}

    public void MarkDisposed()
    {
        _disposed = true;
    }

	void Update() {
		if (Wander != Vector3.zero) {
			Vector3 target = Wander * (float)moveSpeed + transform.position;
			transform.position = Vector3.Lerp (transform.position, target, Time.deltaTime);
		}

        // Check if we have not received an update in 3 seconds - if so, character has likely logged out
        if (_disposed || _initialized && Time.time - _startTime > 3)
        {
            Destroy(gameObject);
        }
	}

    public void UpdateBattle(ClientSettings.BattleEvent battleEvent) {
    	transform.localPosition = battleEvent.BattlePosition;
		Wander = battleEvent.BattleWander;
		
        _initialized = true;
        _startTime = Time.time;

        AssignPlayers(battleEvent.AllyTypes);

        if (_clientSettings == null || _clientSettings.CurrentScene != battleEvent.Scene)
            _disposed = true; // we shouldn't be here...
    }

    public void InitializeBattle(ClientSettings.BattleEvent battleEvent)
    {
        transform.localPosition = battleEvent.BattlePosition;
        Wander = battleEvent.BattleWander;

        AssignPlayers(battleEvent.AllyTypes);
    }

    private void AssignPlayers(List<int> allyTypes)
    {
        bool newAlly = false;

        if (_allyTypes == null || _allyTypes.Count != allyTypes.Count)
        {
            _allyTypes = allyTypes;
            newAlly = true;
        }
        else
        {
            for (int index = 0; index < allyTypes.Count; index++)
            {
                if (_allyTypes[index] != allyTypes[index])
                {
                    _allyTypes = allyTypes;
                    newAlly = true;
                    break;
                }
            }
        }

        if (newAlly)
        {
            GameObject playerA = transform.Find("PlayerA").gameObject;
            GameObject playerB = transform.Find("PlayerB").gameObject;
            GameObject playerC = transform.Find("PlayerC").gameObject;
            GameObject playerD = transform.Find("PlayerD").gameObject;

            SpriteRenderer spriteA = playerA.GetComponent<SpriteRenderer>();
            SpriteRenderer spriteB = playerB.GetComponent<SpriteRenderer>();
            SpriteRenderer spriteC = playerC.GetComponent<SpriteRenderer>();
            SpriteRenderer spriteD = playerD.GetComponent<SpriteRenderer>();

            if (allyTypes.Count > 0)
                spriteA.sprite = GetSprite(allyTypes[0]);
            else
                spriteA.sprite = null;

            if (allyTypes.Count > 1)
                spriteB.sprite = GetSprite(allyTypes[1]);
            else
                spriteB.sprite = null;

            if (allyTypes.Count > 2)
                spriteC.sprite = GetSprite(allyTypes[2]);
            else
                spriteC.sprite = null;

            if (allyTypes.Count > 3)
                spriteD.sprite = GetSprite(allyTypes[3]);
            else
                spriteD.sprite = null;
        }
    }

    private Sprite GetSprite(int characterTypeID)
    {
        string resourceName = "Textures/Icons/Shield";

        switch (characterTypeID)
        {
            case 1:
                resourceName = "Textures/Icons/Shield";
                break;
            case 2:
                resourceName = "Textures/Icons/Sword";
                break;
            default:
                resourceName = "Textures/Icons/Sword";
                break;
        }

        return Resources.Load<Sprite>(resourceName);
    }
}
