using UnityEngine;
using System.Collections;

public class NPC : MovingEntity {
	public int NPCID;
	public int TargetScene;

    NPCData _data = null;
    private bool _showAction;
    private bool _loadDialog;
    private int _questID = 0;
    private ClientSettings _clientSettings;

    public void Initialize (int npcID, int scene, float positionX, float positionY)
	{
        _clientSettings = FindObjectOfType<ClientSettings>();
        
        NPCID = npcID;
		TargetScene = scene;
		
        SetServerPosition(new Vector3(positionX, positionY, 0));

        _showAction = false;
        _loadDialog = false;
        _questID = -1;

        _data = GameServer.NPCSettings[npcID];
        
        // Resources.LoadAll<Sprite>(data.Resource) would load a sprite set
		SpriteRenderer sr = GetComponent<SpriteRenderer> ();
		sr.sprite = Resources.Load<Sprite>(_data.Resource);
	}

    protected void OnGUI()
    {
        if (_showAction)
        {
            Vector3 buttonTarget = new Vector3(transform.position.x, transform.position.y, transform.position.z);
            Vector3 screenPos = Camera.main.WorldToScreenPoint(buttonTarget);
            //Vector2 guiPos = GUIUtility.ScreenToGUIPoint(new Vector2(screenPos.x, screenPos.y));

            if (GUI.Button(new Rect(screenPos.x, (Screen.height - screenPos.y) - (float)15, 75, 75), "?"))
            {
                _showAction = false;
                _loadDialog = true;

                _clientSettings.DialogQueue.Clear();
                GameServer.RetrieveDialog(NPCID);
            }
        }
        if (_loadDialog)
        {
            if (_questID == -1)
            {
                Rect dialogRect = new Rect(Screen.width * .1f, Screen.height * .1f, Screen.width * .8f, Screen.height * .3f);
                GUI.Box(dialogRect, string.Empty);
                GUI.Label(dialogRect, "Loading...");
            }
            else
            {
                GameObject newObject = GameServer.ClientSettings.RetrieveCharacterPrefab(6);

                Dialog dialog = newObject.GetComponent<Dialog>();

                if (_questID > 0)
                {
                    dialog.Initialize(GameServer.QuestDialog[NPCID][_questID], NPCID);
                }
                else
                {
                    dialog.Initialize(_data.Dialog, NPCID);
                }
                _loadDialog = false;
            }
        }
    }

    public void StartDialog(int questID)
    {
        _questID = questID;
    }

    public void ShowAction()
    {
        _showAction = true;
    }

    public void HideAction()
    {
        _showAction = false;
    }

	public void UpdateStats (int scene, float positionX, float positionY)
	{
		TargetScene = scene;
		Vector3 newLocation = new Vector3 (positionX, positionY, 0);

		SetServerPosition (newLocation);
	}
}

public class NPCData {
	public int NPCID;
	public string Resource;
	public DialogData[] Dialog;
}
