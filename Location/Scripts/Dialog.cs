﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Location.Scripts;
using Assets.Settings.Scripts;

public class Dialog : MonoBehaviour
{
    private int _index;
    private int _npcID;
    private DialogData[] _dialog;
    PlayerController _player;
    BattleMenu _battle;
    private ClientSettings _clientSettings = null;
	
    int answerInt = 0;
    Vector2 scrollPosition = Vector2.zero;

    private bool _loadingStore = false;
    private bool _loadingQuest = false;

    private bool _purchaseItem = false;
    private bool _sellItem = false;
    private bool _notEnoughGold = false;


    private Quest _questDetail = null;
    private Quest _questReward = null;
    private InventoryItem _storeDetail;
    private InventoryItem _sellDetail;
    
    private List<InventoryItem> _storeInventory;
    private List<InventoryItem> _sellInventory;
    private List<Quest> _questList;

    GUIStyle labelStyle = null;
    GUIStyle buttonStyle = null;
    private bool _skinInitialized = false;

    protected void Start()
    {
        _clientSettings = FindObjectOfType<ClientSettings>();
        _clientSettings.StoreInventoryQueue.Clear();
                
        GameObject playerGO = GameObject.Find("player");
        Camera battleGO = Camera.main;
        if (playerGO != null)
        {
            _player = playerGO.GetComponent<PlayerController>();
            _player.EnterDialog();
        }
        else if (battleGO != null)
        {
            _battle = battleGO.GetComponent<BattleMenu>();
            _battle.EnterDialog();
        }
    }

    public void Initialize(DialogData[] dialog, int npcID)
    {
        _dialog = dialog;
        _index = 0;
        _npcID = npcID;

        _storeInventory = new List<InventoryItem>();
        _sellInventory = new List<InventoryItem>();
        _questList = null;
    }

    protected void OnGUI()
    {
        if (!_skinInitialized)
        {
            labelStyle = GUI.skin.label;
            buttonStyle = GUI.skin.button;

            // Custom formatting for small screen sizes
            if (Screen.width < 768)
            {
                labelStyle.fontSize = 15.DpToPixel();
                buttonStyle.fontSize = 15.DpToPixel();
                GUI.skin.verticalScrollbar.fixedWidth = Screen.width * 0.05f;
                GUI.skin.verticalScrollbarThumb.fixedWidth = Screen.width * 0.05f;
            }
            else
            {
                // Default styles
            }

            _skinInitialized = true;
        }
        Rect dialogRect = new Rect(Screen.width * .1f, Screen.height * .1f, Screen.width * .8f, Screen.height * .3f);
        
        if (_dialog == null)
        {
            GUI.Box(dialogRect, string.Empty);
            GUI.Label(dialogRect, "Loading...", labelStyle);
            return;
        }

        DialogData currDialog = _dialog[_index];
        string finishText = "...";
        if (currDialog.Next == -1)
        {
            finishText = "OK";
        }
        if (currDialog.Type == DialogData.DialogType.Store)
        {
            StoreGUI(currDialog);
            finishText = "DONE";
        }
        else if (currDialog.Type == DialogData.DialogType.Quest)
        {
            QuestGUI(currDialog);
            finishText = "DONE";
        }
        else
        {
            GUI.Box(dialogRect, string.Empty);
            GUI.Label(dialogRect, currDialog.Text, labelStyle);
        }

        if (currDialog.Type == DialogData.DialogType.Question)
        {
            string[] selStrings = new string[currDialog.Answers.Length];
            for (int index = 0; index < selStrings.Length; index++)
            {
                selStrings[index] = currDialog.Answers[index].Answer;
            }

            answerInt = GUI.Toolbar(new Rect(Screen.width * .1f, Screen.height * .4f, Screen.width * .8f, Screen.height * .1f), answerInt, selStrings);

            if (GUI.changed)
            {
                _index = currDialog.Answers[answerInt].Next;
                answerInt = 0;
            }
        }
        else if (GUI.Button(new Rect(Screen.width * .7f, Screen.height * .5f, Screen.width * .2f, Screen.height * .1f), finishText, buttonStyle))
        {
            if (currDialog.Next == -1 || currDialog.Type == DialogData.DialogType.Store || currDialog.Type == DialogData.DialogType.Quest)
            {
                Destroy(this.gameObject);
                if (_player != null)
                    _player.ExitDialog();
                if (_battle != null)
                    _battle.ExitDialog();
            }
            else
            {
                _index = currDialog.Next;
            }
        }
    }


    private void QuestGUI(DialogData currDialog)
    {
        Rect dialogRect = new Rect(Screen.width * .1f, Screen.height * .1f, Screen.width * .8f, Screen.height * .3f);

        GUI.Box(dialogRect, string.Empty);

        if (!_loadingQuest)
        {
            _clientSettings.QuestQueue.Clear();
            GameServer.RetrieveQuests(_npcID);

            _loadingQuest = true;
        }

        if (_questList == null)
            GUI.Label(dialogRect, "Loading quests: " + _npcID.ToString() + "...", labelStyle);
        else if (_questList.Count == 0)
        {
            GUI.Label(dialogRect, currDialog.Text, labelStyle);
        }
        else
        {
            int rowCount = _questList.Count;

            if (_questDetail != null)
            {
                string reward = _questDetail.Reward;
                if (reward == null)
                {
                    reward = (_clientSettings.CharacterTypeID == 1 ? _questDetail.WoodsmanReward : _questDetail.MerchantReward);
                }

                string questDescription = string.Format("{0} \nReward:{1}\n", _questDetail.QuestDescription, reward);

                if (_questDetail.QuestStatus == Quest.QuestStatusEnum.Completed)
                    questDescription += "(Already finished...)";
                else if (_questDetail.QuestStatus == Quest.QuestStatusEnum.Started)
                    questDescription += "(In Progress...)";
                else if (!_questDetail.Available)
                    questDescription += "(You aren't ready for this...)";

                Rect questDetailRect = new Rect(Screen.width * .1f, Screen.height * .4f, Screen.width * .6f, Screen.height * .4f);
                GUI.Box(questDetailRect, string.Empty);

                GUI.Label(questDetailRect, questDescription, labelStyle);
                
                if (_questDetail.Available)
                {
                    if (GUI.Button(new Rect(Screen.width * .7f, Screen.height * .6f,
                                  Screen.width * .2f, Screen.height * .1f), "Accept", buttonStyle))
                    {
                        _questDetail.QuestStatus = Quest.QuestStatusEnum.Started;
                        _questDetail.Available = false;
                        GameServer.StartQuest(_npcID, _questDetail.QuestID);
                    }
                }
            }

            if (_questReward != null)
            {
                GUI.Label(new Rect(Screen.width * .1f, Screen.height * .5f,
                              Screen.width * .2f, Screen.height * .3f), "I can't thank you enough!" + _questReward.QuestName, labelStyle);
            }
    
            scrollPosition = GUI.BeginScrollView(new Rect(Screen.width * .1f, Screen.height * .11f, Screen.width * .8f, Screen.height * .28f), scrollPosition,
                                                        new Rect(0, 0, Screen.width * .7f, rowCount * Screen.height * .2f));

            int questRow = 0;
            foreach (Quest quest in _questList)
            {
                if (quest.QuestStatus == Quest.QuestStatusEnum.NotStarted || quest.QuestStatus == Quest.QuestStatusEnum.Completed)
                {
                    if (GUI.Button(new Rect(Screen.width * .1f, questRow * Screen.height * .15f,
                                  Screen.width * .6f, Screen.height * .1f), quest.QuestName, buttonStyle))
                    {
                        _questDetail = quest;
                        _questReward = null;
                    }
                }
                else if (quest.Finished)
                {
                    GUI.Label(new Rect(Screen.width * .1f, questRow * Screen.height * .15f,
                                  Screen.width * .2f, Screen.height * .1f), quest.QuestName, labelStyle);

                    if (GUI.Button(new Rect(Screen.width * .5f, questRow * Screen.height * .15f,
                                  Screen.width * .2f, Screen.height * .1f), "Get Reward", buttonStyle))
                    {
                        _questDetail = null;
                        _questReward = quest;
                        _questReward.QuestStatus = Quest.QuestStatusEnum.Completed;
                        GameServer.CompleteQuest(_npcID, quest.QuestID);
                    }
                }
                else
                {
                    GUI.Label(new Rect(Screen.width * .05f, questRow * Screen.height * .15f,
                                  Screen.width * .3f, Screen.height * .1f), string.Format("{0} - {1}", quest.QuestName, quest.StatusDescription), labelStyle);

                    if (GUI.Button(new Rect(Screen.width * .45f, questRow * Screen.height * .15f,
                                  Screen.width * .25f, Screen.height * .1f), "More Info", buttonStyle))
                    {
                        _questDetail = quest;
                        _questReward = null;
                    }
                }

                questRow++;
            }

            GUI.EndScrollView();
        }
    }

    private bool buyMode = true;
    private void StoreGUI(DialogData currDialog)
    {
        string buySellText = buyMode ? "SELL" : "BUY";
        if (GUI.Button(new Rect(Screen.width * .7f, Screen.height * .6f, 
            Screen.width * .2f, Screen.height * .1f), buySellText, buttonStyle))
        {
            buyMode = !buyMode;
        }

        Rect dialogRect = new Rect(Screen.width * .1f, Screen.height * .1f, Screen.width * .8f, Screen.height * .3f);
        Rect storeDetailRect = new Rect(Screen.width * .1f, Screen.height * .45f, Screen.width * .4f, Screen.height * .4f);
        GUI.Box(dialogRect, string.Empty);
        GUI.Box(storeDetailRect, string.Empty);
        
        Rect goldRect = new Rect(Screen.width * .1f, Screen.height * .75f, Screen.width * .4f, Screen.height * .3f);

        
        if (!_loadingStore)
        {
            _clientSettings.StoreInventoryQueue.Clear();
            GameServer.RetrieveStoreInventory(currDialog.StoreID, _npcID);

            SetupSellInventory();

            _loadingStore = true;
        }

        GUI.Label(goldRect, "Current Gold: " + _clientSettings.Gold, labelStyle);

        if (buyMode)
        {
            BuyModeDialog(currDialog);
        }
        else
        {
            SellModeDialog(currDialog);
        }
    }

    private void SellModeDialog(DialogData currDialog)
    {
        Rect dialogRect = new Rect(Screen.width * .1f, Screen.height * .1f, Screen.width * .8f, Screen.height * .3f);
        Rect storeDetailRect = new Rect(Screen.width * .1f, Screen.height * .45f, Screen.width * .4f, Screen.height * .4f);
        if (_sellDetail != null)
        {
            string storeDescription = string.Format("{0} - {1} gold\n", _sellDetail.Description, _sellDetail.Price);

            GUI.Label(storeDetailRect, storeDescription, labelStyle);

            if (GUI.Button(new Rect(Screen.width * .55f, Screen.height * .5f,
                            Screen.width * .15f, Screen.height * .1f), "OK", buttonStyle))
            {
                _sellItem = true;
                _clientSettings.Gold += _sellDetail.Price;

                foreach (var item in _clientSettings.Inventory)
                {
                    if (item.AbilityID == _sellDetail.AbilityID)
                        item.Quantity--;
                }
                GameServer.SellItem(_npcID, currDialog.StoreID, _sellDetail.AbilityID);
                _sellDetail = null;

                SetupSellInventory();
            }
        }


        if (_sellItem)
        {
            GUI.Label(dialogRect, "Please come again!", labelStyle);

            if (GUI.Button(new Rect(Screen.width * .5f, Screen.height * .7f,
                            Screen.width * .4f, Screen.height * .1f), "Sell More", buttonStyle))
            {
                _sellItem = false;
            }
        }
        else
        {
            int rowCount = _sellInventory.Count;

            scrollPosition = GUI.BeginScrollView(new Rect(Screen.width * .1f, Screen.height * .11f, Screen.width * .8f, Screen.height * .28f), scrollPosition,
                                                        new Rect(0, 0, Screen.width * .7f, rowCount * Screen.height * .15f));

            int storeRow = 0;
            foreach (var item in _sellInventory)
            {
                if (GUI.Button(new Rect(Screen.width * .1f, storeRow * Screen.height * .15f,
                                Screen.width * .6f, Screen.height * .1f), string.Format("{0} - {1}", item.Name, item.Quantity), buttonStyle))
                {
                    _sellDetail = item;
                }

                storeRow++;
            }

            GUI.EndScrollView();

        }
    }

    private void BuyModeDialog(DialogData currDialog)
    {
        Rect dialogRect = new Rect(Screen.width * .1f, Screen.height * .1f, Screen.width * .8f, Screen.height * .3f);
        Rect storeDetailRect = new Rect(Screen.width * .1f, Screen.height * .45f, Screen.width * .4f, Screen.height * .4f);
        if (_storeDetail != null)
        {
            string storeDescription = string.Format("{0}\n", _storeDetail.Description);

            if (_clientSettings.Gold < _storeDetail.Price)
                storeDescription += "(Not Enough money...)";

            GUI.Label(storeDetailRect, storeDescription, labelStyle);

            if (_storeDetail.CharacterTypeID.Contains(_clientSettings.CharacterTypeID))
            {
                if (_clientSettings.Gold < _storeDetail.Price)
                    GUI.enabled = false;

                if (GUI.Button(new Rect(Screen.width * .55f, Screen.height * .8f,
                              Screen.width * .15f, Screen.height * .1f), "OK", buttonStyle))
                {
                    _purchaseItem = true;
                    _clientSettings.Gold -= _storeDetail.Price;

                    bool hasSome = false;
                    foreach (var item in _clientSettings.Inventory)
                    {
                        if (item.AbilityID == _storeDetail.AbilityID)
                        {
                            item.Quantity++;
                            hasSome = true;
                        }
                    }
                    if (!hasSome)
                    {
                        _clientSettings.Inventory.Add(InventoryItem.GetInventoryItem(_storeDetail.AbilityID, 1));
                    }

                    SetupSellInventory();
                    GameServer.PurchaseItem(_npcID, currDialog.StoreID, _storeDetail.AbilityID);
                    _storeDetail = null;
                }

                GUI.enabled = true;
            }
        }

            
        if (_storeInventory.Count == 0)
            GUI.Label(dialogRect, "Loading store: " + currDialog.StoreID.ToString() + "...", labelStyle);
        else if (_notEnoughGold)
        {
            GUI.Label(dialogRect, "Come back when you have more gold!", labelStyle);
        }
        else if (_purchaseItem)
        {
            GUI.Label(dialogRect, "Please come again!", labelStyle);

            if (GUI.Button(new Rect(Screen.width * .5f, Screen.height * .5f,
                            Screen.width * .2f, Screen.height * .1f), "Shop More", buttonStyle))
            {
                _purchaseItem = false;
            }
        }
        else
        {
            int rowCount = _storeInventory.Count;

            scrollPosition = GUI.BeginScrollView(new Rect(Screen.width * .1f, Screen.height * .11f, Screen.width * .8f, Screen.height * .28f), scrollPosition,
                                                        new Rect(0, 0, Screen.width * .7f, rowCount * Screen.height * .15f));

            int storeRow = 0;
            foreach (var item in _storeInventory)
            {
                if (GUI.Button(new Rect(Screen.width * .1f, storeRow * Screen.height * .15f,
                                Screen.width * .6f, Screen.height * .1f), string.Format("{0} - {1}", item.Name, item.Price), buttonStyle))
                {
                    _storeDetail = item;
                }

                storeRow++;
            }

            GUI.EndScrollView();

        }
    }

    private void SetupSellInventory()
    {
        // Reset sell options
        _sellInventory = new List<InventoryItem>();
        foreach (var inventoryItem in _clientSettings.Inventory)
        {
            // Make sure the item is not equipped, and we have extra items
            var extraItems = _clientSettings.HasExtra(inventoryItem);
            if (extraItems != null)
            {
                extraItems.Price = (int)(extraItems.Price * 0.5);
                var shopCost = _clientSettings.Spells.FirstOrDefault(s => s.ShopCostDecrease != null);
                if (shopCost != null)
                {
                    extraItems.Price = (int)(extraItems.Price * (1M + shopCost.ShopCostDecrease[shopCost.Level - 1]));
                }

                _sellInventory.Add(extraItems);
            }
        }
    }

    public void Update()
    {
        while (_clientSettings.StoreInventoryQueue.Count > 0)
        {
            string[] storeInventoryMsg = _clientSettings.StoreInventoryQueue.Pop();

            for (int index = 1; index < storeInventoryMsg.Length; index++)
            {
                string[] inventoryItem = storeInventoryMsg[index].Split('~');
                if (inventoryItem.Length > 1)
                {
                    int itemID = int.Parse(inventoryItem[0]);
                    var item = InventoryItem.GetItem(itemID);
                    
                    int price = int.Parse(inventoryItem[1]);

                    item.Price = price;
                    var shopCost = _clientSettings.Spells.FirstOrDefault(s => s.ShopCostDecrease != null);
                    if (shopCost != null)
                    {
                        item.Price = (int) (item.Price*(1M - shopCost.ShopCostDecrease[shopCost.Level - 1]));
                    }

                    _storeInventory.Add(item);
                }
            }
        } 
        while (_clientSettings.QuestQueue.Count > 0)
        {
            _questList = new List<Quest>();
            string[] questMsg = _clientSettings.QuestQueue.Pop();

            for (int index = 1; index < questMsg.Length; index++)
            {
                string[] questItem = questMsg[index].Split('~');
                if (questItem.Length > 1)
                {
                    int questID = int.Parse(questItem[0]);
                    int status = int.Parse(questItem[1]);
                    int complete = int.Parse(questItem[2]);
                    int available = int.Parse(questItem[3]);

                    var quest = Quest.GetQuest(questID);
                    quest.QuestStatus = (Quest.QuestStatusEnum)status;
                    quest.Available = (available == 1);
                    quest.Finished = (complete == 1);

                    _questList.Add(quest);
                }
            }
        }
    }
}

public class DialogData
{
    public enum DialogType { Text, Question, Store, Quest }

    public DialogType Type;
    public DialogAnswer[] Answers;
    public string Text;
    public int StoreID;
    public int QuestID;
    public int Next;
}

public class DialogAnswer
{
    public string Answer;
    public int Next;
}
