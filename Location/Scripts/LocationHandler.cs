﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class LocationHandler : MapHandler
{
    public GameObject _currentScene;
	
	private List<NPC> _npcs = new List<NPC>();
    private string _location;

	// Use this for initialization
	protected override void Start () {
        base.Start();
        _player.GetComponent<Collider>().enabled = false;
        
        _location = PlayerPrefs.GetString("locationResource");
        if (PlayerPrefs.HasKey("PlayerSceneX")) {
            Vector3 location = new Vector3(PlayerPrefs.GetFloat("PlayerSceneX"), PlayerPrefs.GetFloat("PlayerSceneY"), 0f);
            _player.transform.position = location;
            
            PlayerPrefs.DeleteKey("PlayerSceneX");
            PlayerPrefs.DeleteKey("PlayerSceneY");
        }

        // Load correct resource
        GameObject locationResource = Resources.Load<GameObject>(_location);
        if (_clientSettings.CurrentScene != 0)
        {
            LoadScene(_clientSettings.CurrentScene);
        }
        else
        {
            _currentScene = (GameObject)Instantiate(locationResource);
        }

        _player.GetComponent<Collider>().enabled = true;
        GameServer.EnterLocation();
	}
	
	// Update is called once per frame
    protected void Update()
    {
        _npcs.RemoveAll(npc => npc == null);

        if (_lastLocationRefresh < _clientSettings.ServerTime)
        {
			GameServer.UpdateLocation(_player.transform.position, _clientSettings.CurrentScene);

            _lastLocationRefresh = _clientSettings.ServerTime.AddSeconds(1);
		}

		while( _clientSettings.UpdateNPCQueue.Count != 0) {
			string[] npc = _clientSettings.UpdateNPCQueue.Pop();
			UpdateNPC (npc);
		}

        while (_clientSettings.DialogQueue.Count > 0)
        {
            string[] dialogMsg = _clientSettings.DialogQueue.Pop();

            int dialogID = int.Parse(dialogMsg[1]);
            int npcID = int.Parse(dialogMsg[2]);

            NPC npc = _npcs.FirstOrDefault(n => n.NPCID == npcID);
            if (npc != null)
            {
                npc.StartDialog(dialogID);
            }
        }

        if (_player != null) {
			// Follow player with camera
			Vector3 playerPosition = _player.transform.position;
			playerPosition.z = transform.position.z; // Keep camera z index

			transform.position = playerPosition;

		}
	}

    private void UpdateNPC(string[] msg) {
		if (msg.Length < 5)
			return;

		Int32 npcID = Int32.Parse (msg [1]);
		Int32 scene = Int32.Parse (msg [2]);
		float positionX = float.Parse (msg [3]);
		float positionY = float.Parse (msg [4]);

        if (_clientSettings.CurrentScene == scene)
        {

            NPC npc = _npcs.FirstOrDefault(n => n.NPCID == npcID);
            // Check if we have the player already.
            if (npc == null)
            {
                npc = AddNPC(npcID, scene, positionX, positionY);
            }
            else
            {
                npc.UpdateStats(scene, positionX, positionY);
            }
        }
	}

	private NPC AddNPC(int npcID, int scene, float positionX, float positionY)
	{
		GameObject newObject = _clientSettings.RetrieveCharacterPrefab(5);
		
		NPC npc = newObject.GetComponent<NPC> ();
		npc.Initialize (npcID, scene, positionX, positionY);
		
		_npcs.Add(npc);
		npc.name = "npc "+npcID;
		
		return npc;
	}

	public void LoadScene(int sceneID) {
		Destroy(_currentScene);
		_currentScene = null;

        for (int index = _npcs.Count - 1; index >= 0; index--)
        {
            NPC npc = _npcs[index];

            _npcs.Remove(npc);
            Destroy(npc.gameObject);
            npc = null;
        }
        _clientSettings.CurrentScene = sceneID;
        _clientSettings.ClearOverworld();

        GameObject scenePrefab = null;
        if (sceneID == 0)
            scenePrefab = Resources.Load<GameObject>(_location);
        else
            scenePrefab = Resources.Load<GameObject>(_location + "Scenes/" + sceneID.ToString());

        _currentScene = (GameObject)GameObject.Instantiate(scenePrefab);
	}

	public struct LocationSetting {
		public int LocationID;
        public String Resource;
		public Vector2 OverworldPosition;
        public Vector2 PlayerStart;
	}
}
