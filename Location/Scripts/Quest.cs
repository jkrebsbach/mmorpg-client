﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Location.Scripts
{
    public class Quest
    {
        public enum QuestStatusEnum
        {
            NotStarted = 0,
            Started = 1,
            Completed = 2
        };

        public int QuestID;
        public string QuestName;
        public string QuestDescription;
        public string Reward;
        public string WoodsmanReward;
        public string MerchantReward;
        public QuestStatusEnum QuestStatus;
        public bool Finished;
        public bool Available;

        public static Quest GetQuest(int questID)
        {
            return _questLibrary[questID];
        }

        public string StatusDescription
        {
            get
            {
                switch (QuestStatus)
                {
                    case QuestStatusEnum.NotStarted:
                        return "Not Started";
                    case QuestStatusEnum.Started:
                        return "Started";
                    case QuestStatusEnum.Completed:
                        return "Finished";
                    default:
                        return "Unknown";
                }
            }
        }

        private static Dictionary<int, Quest> _questLibrary = new Dictionary<int, Quest>()
	{
		{ 1, new Quest() {QuestID = 1, QuestName = "Pig Hunt", QuestDescription = "There are pigs and squirrels in the field.  Please hunt down these pigs and bring me three tusks.",
            Reward = "100 gold" }},
		{ 2, new Quest() {QuestID = 2, QuestName = "Exterminate the Spiders", QuestDescription = "Deep in the forest the spiders are out of control.  We need somebody to clear things " +
            "up.  Kill five spiders and come back here afterwards.",
            Reward = "Ligh Armor" }},
		{ 3, new Quest() {QuestID = 3, QuestName = "Talk to Ylyea", QuestDescription = "Deep in the forest Ylyea has been researching some Inari artifacts.  " +
            "I need somebody to go check up on her and see how the research is coming along.",
            Reward = null, WoodsmanReward = "Gun", MerchantReward = "Bow" }},
		{ 4, new Quest() {QuestID = 4, QuestName = "Package for Ylyea", QuestDescription = "Ines brought me this book for Ylyea written in the Inari language.  I need you to bring this to her.",
            Reward = "Explosive Rocks" }},
		{ 5, new Quest() {QuestID = 5, QuestName = "Wyrm Hunt", QuestDescription = "The cave near the fort is full of green wyrms.  Please hunt down these wyrms and bring me three dragon wings.",
            Reward = "200 gold" }},
		{ 6, new Quest() {QuestID = 6, QuestName = "Exterminate the Goblins", QuestDescription = "The cave is very dangerous with all of the goblins.  We need somebody to drive them " +
            "away.  Kill five goblins and come back here afterwards.",
            Reward = "Helmet" }},
		{ 7, new Quest() {QuestID = 7, QuestName = "Talk to Ines", QuestDescription = "Deep in the cave Ines has been researching some Inari artifacts. " +
            "I need somebody to go check on him and see how the research is coming along.",
            Reward = "Slow Powder" }},
		{ 8, new Quest() {QuestID = 8, QuestName = "Strange Artifact", QuestDescription = "The books you brought back talk about the leaves of a specific tree in the forest.  Go bring this book to Ylyea and " +
            "ask her to give you the leaves it is describing.",
            Reward = "300 gold" }},
		{ 9, new Quest() {QuestID = 9, QuestName = "Speedy Delivery", QuestDescription = "With the leaves you found, and the book - we made a wreath.  Please bring this artifact to Ines deep in the cave as soon as possible.",
           Reward = "Ice crystals" }}
	};
    }
}
